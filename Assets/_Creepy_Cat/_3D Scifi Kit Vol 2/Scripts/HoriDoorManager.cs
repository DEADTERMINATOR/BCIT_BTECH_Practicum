﻿using UnityEngine;
using System.Collections;

public class HoriDoorManager : MonoBehaviour {

	public DoorHori door1;
	public DoorHori door2;
    public bool isDoorLocked;

	void OnTriggerEnter()
    {
		if (door1 != null && !isDoorLocked)
        {
			door1.OpenDoor();	
		}

		if (door2 != null && !isDoorLocked)
        {
			door2.OpenDoor();	
		}

	}

    public void LockDoor()
    {
        isDoorLocked = true;
    }

    public void UnlockDoor()
    {
        isDoorLocked = false;
    }
}
