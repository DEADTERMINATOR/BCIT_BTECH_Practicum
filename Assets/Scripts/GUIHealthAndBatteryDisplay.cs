﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GUIHealthAndBatteryDisplay : MonoBehaviour
{
    //All GUI textures are being handled by Unity GUI now.
    /*
    //The texture for the heart icon.
    public Texture2D heartTexture;

    //The texture for the health pack icon.
    public Texture2D healthPackTexture;

    //The texture for the battery icon.
    public Texture2D batteryTexture;
    */

    //The text field that holds the current amount of health the player has.
    public Text currentHealthDisplay;

    //The text field that displays the number of health packs the player has.
    public Text healthPackCountDisplay;

    //The text filed that holds the current percentage and number of batteries the player has.
    public Text currentBatteryDisplay;

    //Getter and setter for the amount of health the player has.
	public int health 
	{
		get 
		{
			return _health;
		}

		set 
		{
			_health = value;
			if (_health <= 0) 
			{
				//TODO: Game Over
			}
		}
	}

    //Getter and setter for the amount of battery power the player has.
	public int totalBatteryAmount 
	{
		get 
		{
			return (_currentBatteryCount - 1) * 100 + _currentBatteryPercentage; //The currentBatteryPercentage accounts for our current battery, so we subtract one off the currentBatteryCount so we only count reserve batteries from those.
		}

		set 
		{
			if (value < 0) 
			{
				_currentBatteryCount = 0;
				_currentBatteryPercentage = 0;
			} 
			else 
			{
				_currentBatteryCount = value / 100 + 1; //The current battery counts, so we add one to account for this returning 0 if they have less than a full battery.
				_currentBatteryPercentage = value % 100;
			}
		}
	}

    //Text intended to inform the player when a game over occurs.
    public Text gameOverText;

    /*
    //The position of the heart texture.
    private Rect heartPosition;

    //The position of the health pack texture.
    private Rect healthPackPosition;

    //The position of the battery texture.
    private Rect batteryPosition;
    */

    //The player's health.
    private int _health = 100;

    //The number of health packs the player is carrying.
    //Health packs can be used to heal during a round, but they only heal to 100 points of health.
	private int _currentHealthPackCount = 0;

    //The percentage left in the current battery.
	private int _currentBatteryPercentage = 100;

    //The number of reserve batteries the player has.
	private int _currentBatteryCount = 1;

    /*
    //The size of the windows at initialization. Stored so we only recalculate the position of the texture if the window size changes.
    private Vector2 windowSize;
    */

    //Controls whether the texture is displayed.
	private static bool _isDisplayed = true;

    //Reference to the wave music control script used to switch which piece of music is playing.
    private WaveMusicControl _waveMusicControl;

    //Tracks how long the game over message has been displayed.
    private float _gameOverTimer = 0.0f;

    //Has the game over state been set?
    private bool _gameOverSet = false;

    //Reference to the player state manager so we can call for the dead state to be set once health reaches zero.
    private PlayerStateManager _playerStateManager;

    //The sound that plays when the player dies.
    private AudioSource _deathScream;


    // Use this for initialization
    void Start ()
    {
        //CalculateRect();
        //windowSize = new Vector2(Screen.width, Screen.height);
        currentHealthDisplay.text = "100";
        healthPackCountDisplay.text = _currentHealthPackCount.ToString();
        currentBatteryDisplay.text = "100% x1";
        _waveMusicControl = GameObject.FindGameObjectWithTag("Player").GetComponent<WaveMusicControl>();
        _playerStateManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateManager>();
        _deathScream = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (_health <= 0)
        {
            gameOverText.text = "Game Over";
            if (!_gameOverSet)
            {
                _deathScream.Play();
                GameObject.FindGameObjectWithTag("Player").GetComponent<StartGameControl>().isGameOver = true;
                _waveMusicControl.TransitionToOutOfArena();
                _gameOverSet = true;
                _playerStateManager.HandleDeath();
            }

            _gameOverTimer += Time.deltaTime;
            if (_gameOverTimer > PlayerInArenaCheck.MAX_GAME_OVER_TIMER)
            {
                SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
            }
        }

        //Use a health pack
        if ((Input.GetKeyDown(KeyCode.F) || Input.GetButtonDown("Left Bumper")) && _currentHealthPackCount > 0)
        {
            if (_health < 100)
            {
                if (_health < 50)
                {
                    _health += 50;
                }
                else
                {
                    _health = 100;
                }
                --_currentHealthPackCount;
            }
        }

        /*
        if (windowSize.x != Screen.width || windowSize.y != Screen.height)
        {
            CalculateRect();
        }
        */

        currentHealthDisplay.text = _health.ToString();
        healthPackCountDisplay.text = _currentHealthPackCount.ToString();
        currentBatteryDisplay.text = _currentBatteryPercentage.ToString() + "% x" + _currentBatteryCount.ToString();
    }

    /*
    void OnGUI()
    {
        if (isDisplayed == true)
        {
            GUI.DrawTexture(heartPosition, heartTexture);
            GUI.DrawTexture(healthPackPosition, healthPackTexture);
            GUI.DrawTexture(batteryPosition, batteryTexture);
        }
    }

    void CalculateRect()
    {
        heartPosition = new Rect(0, Screen.height - heartTexture.height,
            heartTexture.width, heartTexture.height);

        healthPackPosition = new Rect(heartTexture.width + currentHealthDisplay.rectTransform.sizeDelta.x, Screen.height - healthPackTexture.height, //We want to offset by the health display amount, so we get the size of the texture and the size of the number text.
            healthPackTexture.width, healthPackTexture.height);

        batteryPosition = new Rect(0, Screen.height - heartTexture.height - batteryTexture.height,
            batteryTexture.width, batteryTexture.height);
    }
    */

    //Used by the restock room to provide a health pack after the purchase has been authorized.
    public void IncreaseHealthPackCount()
    {
        ++_currentHealthPackCount;
    }

    //Used by the restock room to provide healing after the purchase has been authorized.
    public void IncreaseHealth()
    {
        if (_health > 190)
        {
            _health = 200;
        }
        else
        {
            _health += 10;
        }
    }
}
