﻿using UnityEngine;
using System.Collections;

public class RaycastInfo 
{
    //Getter for the raycast hit.
	public RaycastHit hit
	{
		get
		{
			return _hit;
		}
	}

    //Getter for the result of whether the target can be seen.
	public bool canSeeTarget
	{
		get
		{
			return _canSeeTarget;
		}
	}


    //The raycast hit itself.
	private RaycastHit _hit;

    //Can the intended target of the raycast be seen (did the raycast hit what we were testing for).
	private bool _canSeeTarget;


	public RaycastInfo(RaycastHit hit, bool canSeeTarget)
	{
		_hit = hit;
		_canSeeTarget = canSeeTarget;
	}
}
