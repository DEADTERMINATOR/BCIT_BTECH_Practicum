﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OctreeNode
{
	//The parent of this octree node.
	public OctreeNode parent;

	//Getter for the children of this octree node.
	public OctreeNode[] children
	{
		get
		{
			return _children;
		}
	}

	//Getter for the position of the center of the node.
	public Vector3 position
	{
		get
		{
			return _position;
		}
	}

	//The length from the center of the octree node to one of the sides (i.e. half the length)
	public float halfLength;

	//The items contained within this node of the octree.
	public List<OctreeItem> items = new List<OctreeItem>();

	//The game object that will represent the visual dimensions of this octree node.
	public GameObject nodeGameObject;

	//The line renderer that will be responsible for drawing this node.
	public LineRenderer nodeLineRenderer;

	//Getter for whether this node is a leaf node.
	public bool isLeaf
	{
		get
		{
			return _isLeaf;
		}
	}

	//Getter for this node's Morton code.
	public MortonCode mortonCode
	{
		get
		{
			return _mortonCode;
		}
	}

	//Getter for this node's Morton position.
	public Vector3 mortonPosition
	{
		get
		{
			return _mortonPosition;
		}
	}

	//Getter for this node's neighbour nodes. For debugging purposes so a node and its neighbours can be drawn.
	public List<OctreeNode> neighbours
	{
		get
		{
			return _neighbours;
		}
	}

    //Getter for the cost of travelling to this node.
	public int cost
	{
		get
		{
			return _cost;
		}
	}

	//Holds the children nodes of this node. Initially created as an empty array (with 8 slots as an octree node always has 8 children),
	//and filled in later if necessary.
	private OctreeNode[] _children = new OctreeNode[8];

	//The position of this node in the world.
	private Vector3 _position;

	//Is the node a leaf node (i.e. a node with no children).
	private bool _isLeaf;

	//This node's Morton code. The morton code is used to locate this node in the octree when searching for it in relation for other nodes
	//e.g. When this node is a neighbour of another node.
	private MortonCode _mortonCode;

	//The position used when calculating the Morton code for this node. The actual position values are stored as a floating point numbers, but
	//when calculating the Morton code we want to use unsigned integer numbers. So we convert the actual position by ensuring all position values
	//are positive by adding consistent offsets and setting values to their ceiling integer.
	private Vector3 _mortonPosition;

	//Holds whether this node been drawn already. Used when drawing the nodes for debugging purposes.
	private bool _drawn = false;

	//Holds the neighbours of this node. Calculated just after building the tree and stored for faster access during gameplay.
	private List<OctreeNode> _neighbours = new List<OctreeNode>();

    //The cost of travelling to this node.
	private int _cost;


	public OctreeNode(OctreeNode parent, Vector3 position, float halfLength, List<OctreeItem> possibleItems)
	{
		this.parent = parent;
		_position = position;
		this.halfLength = halfLength;

		OctreeNode thisNode = this;
		_mortonPosition = new Vector3(Mathf.CeilToInt(position.x + MortonOrder.X_OFFSET), Mathf.CeilToInt(position.y), Mathf.CeilToInt(position.z));
		_mortonCode = new MortonCode(MortonOrder.MortonEncode((uint)_mortonPosition.x, (uint)_mortonPosition.y, (uint)_mortonPosition.z), ref thisNode);

		_cost = 2;
	}

    //Recursive method that determines whether an octree item belongs in this node or not, or if it needs to be passed to a child.
	public bool ProcessItem(OctreeItem item)
	{
		if (NodeIntersectsTrigger(ref item.avoidanceTrigger)) //Check that the item intersects with the current node.
		{ 
			if (ReferenceEquals(children[0], null)) //Check that this node doesn't have any children.
			{
                //Adds the item to the collection of items this node possesses and now that this node contains at least one item
                //it sets the travel cost to this node to max integer value.
                PushItem(item); 
				return true;
			} 
			else //If there are children, we pass the item down to them to process it.
			{
				foreach (OctreeNode child in children) 
				{
					child.ProcessItem(item); 
				}
				return true;
			}
		}

		return false;
	}

	//Recursive function that starts with the root and breaks it down, creating the tree.
	//Breaks a node down further if it is not already smaller than the maximum allowed node
	//size and if the node collides with the arena. Otherwise, it's marked as a leaf.
	public void Breakdown(ref BoxCollider arenaTrigger)
	{
		if (halfLength > Octree.MAX_NODE_SIZE.x / 2 && NodeIntersectsTrigger(ref arenaTrigger))
		{
			Split(); //Splits the current node into 8 equal children nodes. 4 on the top and 4 on the bottom.
			Octree.totalNodes += 8;
			foreach (OctreeNode child in children)
			{
				child.Breakdown(ref arenaTrigger);
			}
		} 
		else
		{
			_isLeaf = true;
			Octree.mortonCodes.Add(mortonCode);
		}
	}

    //Recursive method that works its way down to the leaf nodes (the ones used for navigation) and sets
    //the neighbours for those nodes.
	public void SetNeighbourNodes()
	{
		if (!ReferenceEquals(children[0], null))
		{
			foreach (OctreeNode child in children)
			{
				if (!child.isLeaf)
				{
					child.SetNeighbourNodes();
				}
				else
				{
					child.FindNeighbourNodes();
				}
			}
		}
	}

    //Recombines smaller leaf nodes into larger ones (up to a maximum size) if they and their siblings ended up containing
    //no octree items.
	public void CombineEmptyNodes()
	{
		if (!ReferenceEquals(children[0], null))
		{
			bool allChildrenAreLeaves = true;
			foreach (OctreeNode child in children)
			{
				if (!child.isLeaf)
				{
					allChildrenAreLeaves = false;
				}
			}

			if (allChildrenAreLeaves)
			{
				bool childrenHaveItems = false;
				foreach (OctreeNode child in children)
				{
					if (child.items.Count != 0)
					{
						childrenHaveItems = true;
					}
				}
		
				if (!childrenHaveItems)
				{
					DeleteChildren(children);
					EraseChildrenNodes();
					Octree.nodesCombined += 8;
					Octree.totalNodes -= 8;
					_isLeaf = true;
					Octree.mortonCodes.Add(mortonCode);
				}
			}
			else
			{
				foreach (OctreeNode child in children)
				{
					bool checkNodeAgain = false; //Do we need to check this node again?
					if (!child.isLeaf)
					{
						child.CombineEmptyNodes();
						if (child.isLeaf)
						{
							//If one of this node's children that wasn't a leaf is now a leaf after running the combination method on each child,
							//it's possible that this node is now eligible for combination itself. So we run the method again on this node to see
							//if that is the case.
							checkNodeAgain = true;
						}
					}

					if (checkNodeAgain)
					{
						CombineEmptyNodes();
					}
				}
			}
		}
	}

	//Determines if this node intersects with the provided box collider trigger. Used to determine if the node
	//is part of the arena, in which case it needs to be broken down as much as is allowed, or if the node collides
	//with an octree item, which means we need to ignore it in pathfinding.
	public bool NodeIntersectsTrigger(ref BoxCollider trigger)
	{
		Bounds nodeBounds = new Bounds(_position, new Vector3(halfLength * 2, halfLength * 2, halfLength * 2));
		return trigger.bounds.Intersects(nodeBounds);
	}

    //Recursive method that finds the smallest node that contains a given point.
	public OctreeNode FindNodeContainingPoint(Vector3 point)
	{
		if (!ReferenceEquals(children[0], null))
		{
			foreach (OctreeNode child in children)
			{
				if (child.PointInsideNode(point))
				{
					return child.FindNodeContainingPoint(point);
				}
			}
		}
		return this;
	}

    //Is a given point inside this particular node.
	private bool PointInsideNode(Vector3 point)
	{
		if (position.x + halfLength <= point.x || position.x - halfLength >= point.x)
		{
			return false;
		}
		if (position.y + halfLength <= point.y || position.y - halfLength >= point.y)
		{
			return false;
		}
		if (position.z + halfLength <= point.z || position.z - halfLength >= point.z)
		{
			return false;
		}
		return true;
	}

    //Frees up this node's children array. Used when recombining leaf nodes into larger ones.
	public void EraseChildrenNodes()
	{
		_children = new OctreeNode[8];
	}

	//Pushes an octree item into the collection of items that this node owns.
	private void PushItem(OctreeItem item)
	{
		if (!items.Contains(item))
		{
			items.Add(item);
			item.ownerNodes.Add(this);
			_cost = int.MaxValue;
		}

		//if (items.Count > Octree.MAX_OBJECT_LIMIT && halfLength > Octree.MAX_NODE_SIZE.x / 2)
		//{
		//	Split();
		//}
	}

	//Splits a node into 8 equal children.
	private void Split()
	{
		Vector3 positionOffset = new Vector3(halfLength / 2, halfLength / 2, halfLength / 2); //Offset for the top 4 nodes.
		for (int i = 0; i < 4; ++i) //Create the top 4 children nodes.
		{ 
			_children[i] = new OctreeNode (this, _position + positionOffset, halfLength / 2, items);
			positionOffset = Quaternion.Euler(0f, 90f, 0f) * positionOffset; //We want to rotate the vector around the verical axis to point to the remaining nodes.
		}

		positionOffset = new Vector3(halfLength / 2, -halfLength / 2, halfLength / 2); //Offset for the bottom 4 nodes.
		for (int i = 4; i < 8; ++i) //Create the bottom 4 children nodes.
		{ 
			_children[i] = new OctreeNode (this, _position + positionOffset, halfLength / 2, items);
			positionOffset = Quaternion.Euler(0f, 90f, 0f) * positionOffset; //We want to rotate the vector around the verical axis to point to the remaining nodes.
		}

		items.Clear();
	}

    //Deletes this node's child nodes. Used when combining smaller leaf nodes into larger ones.
	public void DeleteChildren(OctreeNode[] obsoleteChildren)
	{
		foreach (OctreeItem item in items) 
		{
			item.ownerNodes = item.ownerNodes.Except(obsoleteChildren).ToList();
		}

		foreach (OctreeNode obsoleteChild in obsoleteChildren)
		{
			Octree.mortonCodes.Remove(mortonCode);
			GameObject.Destroy(obsoleteChild.nodeGameObject);
		}
	}

	//Debug function to draw any leaf nodes that contain items. Leaf nodes that contain items will be avoided by the enemy
	//during pathfinding.
	public void DrawNodesWithItems()
	{
		if (!ReferenceEquals(children[0], null))
		{
			foreach (OctreeNode child in children)
			{
				child.DrawNodesWithItems();
			}
		}
		if (isLeaf && items.Count != 0 && !_drawn)
		{
			_drawn = true;
			//foreach (OctreeItem item in items)
			//{
				//DrawItemBounds(item.avoidanceTrigger.bounds);
			//}
			DrawNodeGameObject(Color.red);
		}
	}

    //Debug function to draw any leaf nodes that do not contain octree items. Leaf nodes that don't contain items will be used by the
    //enemy during pathfinding.
	public void DrawNodesWithoutItems()
	{
		if (!ReferenceEquals(children[0], null))
		{
			foreach (OctreeNode child in children)
			{
				child.DrawNodesWithoutItems();
			}
		}
		if (isLeaf && items.Count == 0 && !_drawn)
		{
			_drawn = true;
            //foreach (OctreeItem item in items)
            //{
            //	DrawItemBounds(item.avoidanceTrigger.bounds);
            //}
            DrawNodeGameObject(Color.blue);
		}
	}

	//Debug function that creates the renderer and game object required to draw the bounds of this node.
	public void DrawNodeGameObject(Color color)
	{
		nodeGameObject = new GameObject();
		nodeGameObject.hideFlags = HideFlags.HideInHierarchy;
		nodeLineRenderer = nodeGameObject.AddComponent<LineRenderer>();

		CreateNodeCorners(ref nodeLineRenderer, new Vector3(halfLength, halfLength, halfLength));
		nodeLineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		nodeLineRenderer.SetColors(color, color);
	}

	//Debug function that creates the renderer and game object required to draw the bounds of the items.
	public void DrawItemBounds(Bounds itemBounds)
	{
		GameObject itemGameObject = new GameObject();
		itemGameObject.hideFlags = HideFlags.HideInHierarchy;
		LineRenderer itemLineRenderer = itemGameObject.AddComponent<LineRenderer>();

		CreateBoundCorners(ref itemLineRenderer, itemBounds);
		itemLineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		itemLineRenderer.SetColors(Color.blue, Color.blue);
	}

	//Debug function to draw the bounds of the nodes of the octree.
	private void CreateNodeCorners(ref LineRenderer lineRenderer, Vector3 halfLength)
	{
		Vector3[] nodeCoordinates = new Vector3[8];

		Vector3 corner = new Vector3(halfLength.x, halfLength.y, halfLength.z); //Offset for the top top right of the node.
		for (int i = 0; i < 4; ++i) //Create the top 4 corners of the node.
		{ 
			nodeCoordinates[i] = _position + corner; //We want the point to be relative to the actual position of the node, and not Vector3.zero.
			corner = Quaternion.Euler(0f, 90f, 0f) * corner; //We want to rotate the vector around the verical axis to point to the remaining corners.
		}

		corner = new Vector3 (halfLength.x, -halfLength.y, halfLength.z); //Offset for the bottom top right of the node.
		for (int i = 4; i < 8; ++i) //Create the bottom 4 corners of the node.
		{
			nodeCoordinates[i] = _position + corner; //We want the point to be relative to the actual position of the node, and not Vector3.zero.
			corner = Quaternion.Euler(0f, 90f, 0f) * corner; //We want to rotate the vector around the verical axis to point to the remaining corners.
		}

		lineRenderer.useWorldSpace = true;
		lineRenderer.SetVertexCount(16);
		lineRenderer.SetWidth(0.03f, 0.03f);

		lineRenderer.SetPosition(0, nodeCoordinates[0]);
		lineRenderer.SetPosition(1, nodeCoordinates[1]);
		lineRenderer.SetPosition(2, nodeCoordinates[2]);
		lineRenderer.SetPosition(3, nodeCoordinates[3]);
		lineRenderer.SetPosition(4, nodeCoordinates[0]);
		lineRenderer.SetPosition(5, nodeCoordinates[4]);
		lineRenderer.SetPosition(6, nodeCoordinates[5]);
		lineRenderer.SetPosition(7, nodeCoordinates[1]);

		lineRenderer.SetPosition(8, nodeCoordinates[5]);
		lineRenderer.SetPosition(9, nodeCoordinates[6]);
		lineRenderer.SetPosition(10, nodeCoordinates[2]);
		lineRenderer.SetPosition(11, nodeCoordinates[6]);
		lineRenderer.SetPosition(12, nodeCoordinates[7]);
		lineRenderer.SetPosition(13, nodeCoordinates[3]);
		lineRenderer.SetPosition(14, nodeCoordinates[7]);
		lineRenderer.SetPosition(15, nodeCoordinates[4]);
	}

	//Debug function to draw the bounds of the items being inserted into the tree.
	private void CreateBoundCorners(ref LineRenderer lineRenderer, Bounds bound)
	{
		Vector3 boundsFrontTopLeft;
		Vector3 boundsFrontTopRight;
		Vector3 boundsFrontBottomLeft;
		Vector3 boundsFrontBottomRight;
		Vector3 boundsBackTopLeft;
		Vector3 boundsBackTopRight;
		Vector3 boundsBackBottomLeft;
		Vector3 boundsBackBottomRight;

		Vector3 center = bound.center;
		Vector3 extents = bound.size / 2;

		boundsFrontTopLeft     = new Vector3(center.x - extents.x, center.y + extents.y, center.z - extents.z);  // Front top left corner
		boundsFrontTopRight    = new Vector3(center.x + extents.x, center.y + extents.y, center.z - extents.z);  // Front top right corner
		boundsFrontBottomLeft  = new Vector3(center.x - extents.x, center.y - extents.y, center.z - extents.z);  // Front bottom left corner
		boundsFrontBottomRight = new Vector3(center.x + extents.x, center.y - extents.y, center.z - extents.z);  // Front bottom right corner
		boundsBackTopLeft      = new Vector3(center.x - extents.x, center.y + extents.y, center.z + extents.z);  // Back top left corner
		boundsBackTopRight     = new Vector3(center.x + extents.x, center.y + extents.y, center.z + extents.z);  // Back top right corner
		boundsBackBottomLeft   = new Vector3(center.x - extents.x, center.y - extents.y, center.z + extents.z);  // Back bottom left corner
		boundsBackBottomRight  = new Vector3(center.x + extents.x, center.y - extents.y, center.z + extents.z);  // Back bottom right corner

		lineRenderer.useWorldSpace = true;
		lineRenderer.SetVertexCount(16);
		lineRenderer.SetWidth(0.03f, 0.03f);

		lineRenderer.SetPosition(0, boundsFrontTopLeft);
		lineRenderer.SetPosition(1, boundsFrontTopRight);
		lineRenderer.SetPosition(2, boundsBackTopRight);
		lineRenderer.SetPosition(3, boundsBackTopLeft);
		lineRenderer.SetPosition(4, boundsFrontTopLeft);
		lineRenderer.SetPosition(5, boundsFrontBottomLeft);
		lineRenderer.SetPosition(6, boundsFrontBottomRight);
		lineRenderer.SetPosition(7, boundsFrontTopRight);

		lineRenderer.SetPosition(8, boundsFrontBottomRight);
		lineRenderer.SetPosition(9, boundsBackBottomRight);
		lineRenderer.SetPosition(10, boundsBackTopRight);
		lineRenderer.SetPosition(11, boundsBackBottomRight);
		lineRenderer.SetPosition(12, boundsBackBottomLeft);
		lineRenderer.SetPosition(13, boundsBackTopLeft);
		lineRenderer.SetPosition(14, boundsBackBottomLeft);
		lineRenderer.SetPosition(15, boundsFrontBottomLeft);
	}

    //Calculates and stores this node's neighbours.
	private void FindNeighbourNodes()
	{
		List<ulong> potentialNeighbours = new List<ulong>();

        //Calculate the Morton Codes for locations where we expect to find neighbours.
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)mortonPosition.y, (uint)mortonPosition.z));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)(mortonPosition.y + 2), (uint)mortonPosition.z));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)mortonPosition.y, (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y + 2), (uint)mortonPosition.z));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)mortonPosition.y, (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)mortonPosition.y, (uint)mortonPosition.z));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)(mortonPosition.y - 2), (uint)mortonPosition.z));

		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)mortonPosition.y, (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y - 2), (uint)mortonPosition.z));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)mortonPosition.y, (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)mortonPosition.x, (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z)));

		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y - 2), (uint)(mortonPosition.z + 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x + 2), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z - 2)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z)));
		potentialNeighbours.Add(MortonOrder.MortonEncode((uint)(mortonPosition.x - 2), (uint)(mortonPosition.y + 2), (uint)(mortonPosition.z + 2)));

		//DrawNodeGameObject(Color.blue);

        //Search the collection of Morton Codes for the expected neighbours. Add the associated nodes as we find them.
		foreach (ulong neighbour in potentialNeighbours)
		{
			MortonCode neighbourCode = Octree.mortonCodes.Find(i => i.mortonCode == neighbour);
			if (neighbourCode != null)
			{
				//neighborCode.associatedNode.DrawNodeGameObject(Color.red);
				_neighbours.Add(neighbourCode.associatedNode);
			}
		}
	}
}
