﻿using UnityEngine;
using System.Collections;

public class GUICrosshair : MonoBehaviour
{
	//The texture used for the crosshair.
    public Texture2D crosshairTexture;


	//The position of the rect that holds the crosshair.
	private Rect _position;

	//The total size of the game window.
	private Vector2 _windowSize;

	//Do we want to display the crosshair.
	private static bool _isDisplayed = true;


	// Use this for initialization
	void Start ()
	{
	    CalculateRect();
        _windowSize = new Vector2(Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (_windowSize.x != Screen.width || _windowSize.y != Screen.height)
	    {
	        CalculateRect();
	    }
	}

    void OnGUI()
    {
        if (_isDisplayed == true)
        {
            GUI.DrawTexture(_position, crosshairTexture);
        }
    }

    void CalculateRect()
    {
        _position = new Rect((Screen.width - crosshairTexture.width) / 2.0f,
            (Screen.height - crosshairTexture.height) / 2.0f,
            crosshairTexture.width, crosshairTexture.height);
    }
}
