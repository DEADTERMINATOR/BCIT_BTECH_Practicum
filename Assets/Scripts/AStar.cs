﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class AStar
{
	public static List<OctreeNode> AStarPathfinding(OctreeNode start, OctreeNode end)
	{
		AStarNode AStarStart = new AStarNode(start);
		AStarNode AStarEnd = new AStarNode(end);

		List<AStarNode> closedSet = new List<AStarNode>();
		List<AStarNode> openSet = new List<AStarNode>();
		openSet.Add(AStarStart);

		AStarStart.gScore = 0f;
		AStarStart.fScore = Heuristic(AStarStart, AStarEnd);

		while (openSet.Count != 0)
		{
			AStarNode current = FindLowestFScore(openSet);
			if (ReferenceEquals(current.node, end))
			{
				return ReconstructPath(current.cameFrom, current);
			}

			openSet.Remove(current);
			closedSet.Add(current);
			List<AStarNode> currentNeighbours = GetNodeNeighbours(current);

			foreach (AStarNode neighbour in currentNeighbours)
			{
				if (closedSet.Contains(neighbour))
				{
					continue;
				}
				float tentativeGScore = current.gScore + neighbour.node.cost;

				if (!openSet.Contains(neighbour))
				{
					openSet.Add(neighbour);
				}
				else if (tentativeGScore >= neighbour.gScore)
				{
					continue;
				}

				neighbour.cameFrom = current;
				neighbour.gScore = tentativeGScore;
				neighbour.fScore = neighbour.gScore + Heuristic(neighbour, AStarEnd);
			}
		}
		return null;
	}

	private static float Heuristic(AStarNode start, AStarNode end)
	{
		return Mathf.Sqrt(Mathf.Pow(start.node.position.x - end.node.position.x, 2) + 
			Mathf.Pow(start.node.position.y - end.node.position.y, 2) + 
			Mathf.Pow(start.node.position.z - end.node.position.z, 2));
	}

	private static AStarNode FindLowestFScore(List<AStarNode> openSet)
	{
		float lowestFScore = float.MaxValue;
		AStarNode lowestFScoreNode = null;

		foreach (AStarNode node in openSet)
		{
			if (node.fScore < lowestFScore)
			{
				lowestFScore = node.fScore;
				lowestFScoreNode = node;
			}
		}

		return lowestFScoreNode;
	}

	private static List<OctreeNode> ReconstructPath(AStarNode cameFrom, AStarNode current)
	{
		List<OctreeNode> totalPath = new List<OctreeNode>();
		totalPath.Add(current.node);
		current = current.cameFrom;

		while (current != null)
		{
			totalPath.Add(current.node);
			current = current.cameFrom;
		}
		return totalPath;
	}

	private static List<AStarNode> GetNodeNeighbours(AStarNode current)
	{
		List<AStarNode> nodeNeighbours = new List<AStarNode>();
		foreach (OctreeNode neighbour in current.node.neighbours)
		{
			nodeNeighbours.Add(new AStarNode(neighbour));
		}
		return nodeNeighbours;
	}
}
