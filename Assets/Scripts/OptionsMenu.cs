﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsMenu : MonoBehaviour
{
	//Reference to the panel that holds the various options.
	private GameObject _optionsPanel;

    //Do the preferences need to be updated in the file.
	private bool _updateNeeded = false;

	//Reference to the menu music audio source so we can immediately change the volume on the menu based on the player's input.
	private AudioSource _menuMusic;


	// Use this for initialization
	void Start ()
    {
        _menuMusic = GameObject.FindGameObjectWithTag("MenuMusic").GetComponent<AudioSource>();

        _optionsPanel = GameObject.FindGameObjectWithTag("OptionsPanel");
        Component[] sliders = _optionsPanel.GetComponentsInChildren<Slider>();
        Component[] toggles = _optionsPanel.GetComponentsInChildren<Toggle>();

        foreach (Slider slider in sliders)
        {
            if (slider.gameObject.tag == "MusicVolume")
            {
                if (PlayerPrefs.HasKey("MusicVolume"))
                {
                    slider.value = PlayerPrefs.GetFloat("MusicVolume");
                }
                else
                {
                    slider.value = 0.8f;
                    PlayerPrefs.SetFloat("MusicVolume", 0.8f);
                }
            }
            else if (slider.gameObject.tag == "AimingSensitivity")
            {
                if (PlayerPrefs.HasKey("AimingSensitivity"))
                {
                    slider.value = PlayerPrefs.GetFloat("AimingSensitivity");
                }
                else
                {
                    slider.value = 0.5f;
                    PlayerPrefs.SetFloat("AimingSensitivity", 0.5f);
                }
            }
            else if (slider.gameObject.tag == "SnapRotateDegrees")
            {
                if (PlayerPrefs.HasKey("SnapRotateDegrees"))
                {
                    slider.value = PlayerPrefs.GetFloat("SnapRotateDegrees");
                }
                else
                {
                    slider.value = 3f;
                    PlayerPrefs.SetFloat("SnapRotateDegrees", 3f);
                }
            }
        }

        foreach (Toggle toggle in toggles)
        {
            if (toggle.gameObject.tag == "FullRotation")
            {
                if (PlayerPrefs.HasKey("FullRotation"))
                {
                    if (PlayerPrefs.GetInt("FullRotation") == 1)
                    {
                        toggle.isOn = true;
                    }
                    else
                    {
                        toggle.isOn = false;
                    }
                }
                else
                {
                    toggle.isOn = false;
                    PlayerPrefs.SetInt("FullRotation", 0);
                }
            }
        }

        PlayerPrefs.Save();
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if (_optionsPanel.activeSelf)
        {
            _updateNeeded = true;
            Component[] sliders = _optionsPanel.GetComponentsInChildren<Slider>();
            Component[] toggles = _optionsPanel.GetComponentsInChildren<Toggle>();

            foreach (Slider slider in sliders)
            {
                if (slider.gameObject.tag == "MusicVolume")
                {
                    PlayerPrefs.SetFloat("MusicVolume", slider.value);
                    _menuMusic.volume = slider.value;
                }
                else if (slider.gameObject.tag == "AimingSensitivity")
                {
                    PlayerPrefs.SetFloat("AimingSensitivity", slider.value);
                }
                else if (slider.gameObject.tag == "SnapRotateDegrees")
                {
                    PlayerPrefs.SetFloat("SnapRotateDegrees", slider.value);
                }
            }

            foreach (Toggle toggle in toggles)
            {
                if (toggle.gameObject.tag == "FullRotation")
                {
                    if (toggle.isOn)
                    {
                        PlayerPrefs.SetInt("FullRotation", 1);
                        //if (GameObject.Find("Snap Rotate Degrees Slider").activeSelf)
                        //{
                            GameObject.Find("Snap Rotate Degrees Slider").GetComponent<Slider>().enabled = false;
                        //}
                    }
                    else
                    {
                        PlayerPrefs.SetInt("FullRotation", 0);
                        GameObject.Find("Snap Rotate Degrees Slider").GetComponent<Slider>().enabled = true;
                    }
                }
            }
        }
        else
        {
            if (_updateNeeded)
            {
                PlayerPrefs.Save();
                _updateNeeded = false;
            }
        }
	}
}
