﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Waypoint : MonoBehaviour {

    //The initial radius for waypoints to check for neighbours.
	private const int _STARTING_RADIUS = 5;

    //The amount the radius increases everytime this waypoint is incapable of finding a suitable
    //number of neighbours.
	private const int _RADIUS_INCREASE = 2;

    //The minimum number of neighbours a waypoint should try to look for.
	private const int _MINIMUM_NEIGHBOURS = 5;


    //Is this waypoint directly connected to another? If it is, then this waypoint doesn't
    //receive a list of neighbours, as we want the enemy to go directly to the connected waypoint.
    //Example use would be the waypoints that connect the staircases.
    public Waypoint connectedWaypoint = null;

    //If this waypoint is not directly connected to another, it maintains a list of nearby waypoints.
    public List<Waypoint> neighbours;

    //The layer mask that determines which waypoints this waypoint should consider as neighbours.
    //For example, waypoints on the first floor should not be neighbours with those on the second floor,
    //and no ground waypoint should be neighbours with an air waypoint.
    public LayerMask mask;

    //Is this waypoint a designated cover point. Any waypoint surrounding a barrier can be a cover point,
    //the enemy will raycast to determine if it's valid if they are considering it.
    public bool isCover;

    //Is the waypoint currently occupied by an enemy. If so, other enemies should look for another waypoint to
    //use. We don't want enemies piling up on the same waypoint.
    [HideInInspector]
    public bool isOccupied = false;


    //A count that maintains how many times the neighbour check has run. Used to break the loop if necessary
    //so the game doesn't run into an infinite loop.
	private int _neighbourBreak = 1;


	// Use this for initialization
	void Start ()
    {
        int radius = _STARTING_RADIUS;
        Collider[] neighbourColliders = Physics.OverlapSphere(transform.position, radius, mask);
        int waypointsFound = neighbourColliders.Length;
        radius += _RADIUS_INCREASE;
        ++_neighbourBreak;
        while (waypointsFound < _MINIMUM_NEIGHBOURS && _neighbourBreak < 10)
        {
            neighbourColliders = Physics.OverlapSphere(transform.position, radius, mask);
            waypointsFound = neighbourColliders.Length;
            radius += _RADIUS_INCREASE;
            ++_neighbourBreak;
        }

        for (int i = 0; i < neighbourColliders.Length; ++i)
        {
            neighbours.Add(neighbourColliders[i].gameObject.GetComponent<Waypoint>());
        }

        if (connectedWaypoint != null)
        {
            neighbours.Add(connectedWaypoint);
        }
	}
}
