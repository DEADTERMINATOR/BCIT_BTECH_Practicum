﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SelectOnInput : MonoBehaviour
{
    //Event system so this script can be connected to the GUI.
    public EventSystem eventSystem;

    //The game object (button, slider etc) that is currently selected.
    public GameObject selectedObject;


    //Is the button already selected.
	private bool _buttonSelected = false;

	
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && _buttonSelected == false)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            _buttonSelected = true;
        }	
	}

    private void OnDisable()
    {
        _buttonSelected = false;
    }
}
