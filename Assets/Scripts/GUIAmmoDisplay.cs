﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIAmmoDisplay : MonoBehaviour
{
    //Enum holding the possible weapons the player could be holding.
    public enum Weapon { PISTOL, REAPER, ASSAULT_RIFLE, LASER_RIFLE };

    //The maximum magazine size for each type of weapon.
	private const int _MAX_PISTOL_MAGAZINE = 12;
	private const int _MAX_REAPER_MAGAZINE = 50;
	private const int _MAX_ASSAULT_RIFLE_MAGAZINE = 30;
	private const int _MAX_LASER_RIFLE_MAGAZINE = 1;


    //The text field that holds the current amount of ammo in the magazine and the reserve ammo.
    public Text currentWeaponAmmo;

    //Reference to the pistol ammo pickup. We use this to automatically spawn more ammo for the player when they reload, as pistol ammo should be unlimited.
    public GameObject pistolAmmoPickup;

    //The current weapon the player is wielding.
    [HideInInspector]
    public Weapon currentWeapon = Weapon.PISTOL;

    //Holds the current amount of ammo in the pistol magazine, as well as how much ammo they have in reserve.
	private int _currentPistolMagazine = _MAX_PISTOL_MAGAZINE;
	private int _reservePistolAmmo = 999;

    //Holds the current amount of ammo in the reaper magazine, as well as how much ammo they have in reserve.
	private int _currentReaperMagazine = _MAX_REAPER_MAGAZINE;
	private int _reserveReaperAmmo = 50;

    //Holds the current amount of ammo in the assault rifle magazine, as well as how much ammo they have in reserve.
	private int _currentAssaultRifleMagazine = _MAX_ASSAULT_RIFLE_MAGAZINE;
	private int _reserveAssaultRifleAmmo = 30;

    //Holds the current amount of ammo in the laser rifle magazine, as well as how many shots they have in reserve.
	private int _currentLaserRifleMagazine = _MAX_LASER_RIFLE_MAGAZINE;
	private int _reserveLaserRifleAmmo = 4;

    //Reference to the player in restock room script used to check if a weapon is owned before switching the current weapon.
	private PlayerInRestockRoom _weaponOwnedCheck;

    //Reference to the player state manager used to see if the player is currently reloading when we switch weapons.
	private PlayerStateManager _playerStateManager;

    //References to the player's weapons to check which one is equipped.
    private GameObject _pistol;
    private GameObject _reaper;
    private GameObject _assaultRifle;
    private GameObject _laserRifle;


    // Use this for initialization
    void Start()
    {
        _weaponOwnedCheck = GameObject.FindGameObjectWithTag("RestockRoom").GetComponent<PlayerInRestockRoom>();
        _playerStateManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateManager>();

        _pistol = GameObject.FindGameObjectWithTag("Pistol");
        _reaper = GameObject.FindGameObjectWithTag("Reaper");
        _assaultRifle = GameObject.FindGameObjectWithTag("AssaultRifle");
        _laserRifle = GameObject.FindGameObjectWithTag("LaserRifle");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!_playerStateManager.reloading)
        {
            if (_pistol.activeSelf)
            {
                currentWeapon = Weapon.PISTOL;
            }
            else if (_reaper.activeSelf && _weaponOwnedCheck.reaperPurchased)
            {
                currentWeapon = Weapon.REAPER;
            }
            else if (_assaultRifle.activeSelf && _weaponOwnedCheck.assaultRiflePurchased)
            {
                currentWeapon = Weapon.ASSAULT_RIFLE;
            }
            else if (_laserRifle.activeSelf && _weaponOwnedCheck.laserRiflePurchased)
            {
                currentWeapon = Weapon.LASER_RIFLE;
            }
        }

        if (currentWeapon == Weapon.PISTOL)
        {
            currentWeaponAmmo.text = _currentPistolMagazine.ToString() + "/" + _reservePistolAmmo.ToString();
        }
        else if (currentWeapon == Weapon.REAPER)
        {
            currentWeaponAmmo.text = _currentReaperMagazine.ToString() + "/" + _reserveReaperAmmo.ToString();
        }
        else if (currentWeapon == Weapon.ASSAULT_RIFLE)
        {
            currentWeaponAmmo.text = _currentAssaultRifleMagazine.ToString() + "/" + _reserveAssaultRifleAmmo.ToString();
        }
        else if (currentWeapon == Weapon.LASER_RIFLE)
        {
            currentWeaponAmmo.text = _currentLaserRifleMagazine.ToString() + "/" + _reserveLaserRifleAmmo.ToString();
        }
	}

    public void FireBullet()
    {
        if (currentWeapon == Weapon.PISTOL)
        {
            --_currentPistolMagazine;
        }
        else if (currentWeapon == Weapon.REAPER)
        {
            --_currentReaperMagazine;
        }
        else if (currentWeapon == Weapon.ASSAULT_RIFLE)
        {
            --_currentAssaultRifleMagazine;
        }
        else if (currentWeapon == Weapon.LASER_RIFLE)
        {
            --_currentLaserRifleMagazine;
        }
    }

    public void ReloadMagazine()
    {
        int spentAmmo, newReserveAmmo;
        if (currentWeapon == Weapon.PISTOL)
        {
            _currentPistolMagazine = _MAX_PISTOL_MAGAZINE;
            Instantiate(pistolAmmoPickup, GameObject.FindGameObjectWithTag("Player").transform.position, Quaternion.identity);
        }
        else if (currentWeapon == Weapon.REAPER)
        {
            spentAmmo = _MAX_REAPER_MAGAZINE - _currentReaperMagazine;
            newReserveAmmo = _reserveReaperAmmo - spentAmmo;

            if (_reserveReaperAmmo > _MAX_REAPER_MAGAZINE || spentAmmo < _reserveReaperAmmo)
            {
                _currentReaperMagazine = _MAX_REAPER_MAGAZINE;
                _reserveReaperAmmo = newReserveAmmo;
            }
            else
            {
                _currentReaperMagazine += _reserveReaperAmmo;
                _reserveReaperAmmo = 0;
            }
        }
        else if (currentWeapon == Weapon.ASSAULT_RIFLE)
        {
            spentAmmo = _MAX_ASSAULT_RIFLE_MAGAZINE - _currentAssaultRifleMagazine;
            newReserveAmmo = _reserveAssaultRifleAmmo - spentAmmo;

            if (_reserveAssaultRifleAmmo > _MAX_ASSAULT_RIFLE_MAGAZINE || spentAmmo < _reserveAssaultRifleAmmo)
            {
                _currentAssaultRifleMagazine = _MAX_ASSAULT_RIFLE_MAGAZINE;
                _reserveAssaultRifleAmmo = newReserveAmmo;
            }
            else
            {
                _currentAssaultRifleMagazine += _reserveAssaultRifleAmmo;
                _reserveAssaultRifleAmmo = 0;
            }
        }
        else if (currentWeapon == Weapon.LASER_RIFLE)
        {
            spentAmmo = _MAX_LASER_RIFLE_MAGAZINE - _currentLaserRifleMagazine;
            newReserveAmmo = _reserveLaserRifleAmmo - spentAmmo;

            if (_reserveLaserRifleAmmo > _MAX_LASER_RIFLE_MAGAZINE || spentAmmo < _reserveLaserRifleAmmo)
            {
                _currentLaserRifleMagazine = _MAX_LASER_RIFLE_MAGAZINE;
                _reserveLaserRifleAmmo = newReserveAmmo;
            }
            else
            {
                _currentLaserRifleMagazine += _reserveLaserRifleAmmo;
                _reserveLaserRifleAmmo = 0;
            }
        }
    }

    public void IncreaseReserves(Weapon weapon)
    {
        if (weapon == Weapon.REAPER)
        {
            _reserveReaperAmmo += _MAX_REAPER_MAGAZINE;
        }
        else if (weapon == Weapon.ASSAULT_RIFLE)
        {
            _reserveAssaultRifleAmmo += _MAX_ASSAULT_RIFLE_MAGAZINE;
        }
        else if (weapon == Weapon.LASER_RIFLE)
        {
            _reserveLaserRifleAmmo += _MAX_LASER_RIFLE_MAGAZINE;
        }
    }
}
