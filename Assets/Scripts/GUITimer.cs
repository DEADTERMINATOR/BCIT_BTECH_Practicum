﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUITimer : MonoBehaviour
{
    //The amount of time either the gravity or non-gravity state is active for.
    public const float STATE_ACTIVE_TIME = 60f;


    //The timer text field. Used to either report the amount of time until the next gravity switch,
    //or to report the amount of time until the next wave (as gravity does not switch in-between waves.
    public Text timer;

	//Holds whether a gravity switch is needed.
	public bool switchNeeded 
	{
		get 
		{
			return _switchNeeded;
		}

		set 
		{
			_switchNeeded = value;
		}
	}


    //Is the wave current active.
    private bool _isWaveActive;

    //The time until the next gravity state switch.
	private float _timeTillSwitch = STATE_ACTIVE_TIME;

    //Whether the gravity state needs to be switched.
	private bool _switchNeeded = false;

    //The enemy spawner script.
	private EnemySpawn _enemySpawn;

    //The gravity switch script. Used to get the current gravity state and display the appropriate text.
	private GravitySwitch _gravitySwitch;


	// Use this for initialization
	void Start ()
	{
	    timer.text = "Time Until Gravity Switch: 2:00";
        _enemySpawn = GameObject.FindGameObjectWithTag("Player").GetComponent<EnemySpawn>();
        _gravitySwitch = GameObject.FindGameObjectWithTag("Player").GetComponent<GravitySwitch>();
    }
	
	// Update is called once per frame
	void Update ()
	{
		_isWaveActive = _enemySpawn.waveStatus;
        //If the wave is active, we track and display the gravity switch countdown. Otherwise, we track and display
        //the time until the next wave.
        if (_isWaveActive)
        {
            _timeTillSwitch -= Time.deltaTime;
            if (_timeTillSwitch < 1f)
            {
                _timeTillSwitch = STATE_ACTIVE_TIME;
                timer.text = "Time Until Gravity Switch: 2:00";
                _switchNeeded = true;
            }
            else
            {
                int secondsRemaining = (int)_timeTillSwitch % 60;
                int minutesRemaining = (int)Math.Floor(_timeTillSwitch / 60);
                if (secondsRemaining < 10)
                {
                    if (_gravitySwitch.gravityOn)
                    {
                        timer.text = "Gravity Switches Off: " + minutesRemaining + ":0" + secondsRemaining;
                    }
                    else
                    {
                        timer.text = "Gravity Switches On: " + minutesRemaining + ":0" + secondsRemaining;
                    }
                }
                else
                {
                    if (_gravitySwitch.gravityOn)
                    {
                        timer.text = "Gravity Switches Off: " + minutesRemaining + ":" + secondsRemaining;
                    }
                    else
                    {
                        timer.text = "Gravity Switches On: " + minutesRemaining + ":" + secondsRemaining;
                    }
                }
            }
        }
        else
        {
			int minutesRemaining = (int)Math.Floor((_enemySpawn.timeBetweenWaves - _enemySpawn.betweenWaveTimer) / 60);
			int secondsRemaining = (int)(_enemySpawn.timeBetweenWaves - _enemySpawn.betweenWaveTimer) % 60;
            if (secondsRemaining < 10)
            {
                timer.text = "Time Until Next Wave " + minutesRemaining + ":0" + secondsRemaining;
            }
            else
            {
                timer.text = "Time Until Next Wave: " + minutesRemaining + ":" + secondsRemaining;
            }
        }
	}

    //Resets the switch timer back to the top. Used when a wave is ended as we want the next wave to begin with the
    //timer reset.
    public void ResetTimeTillSwitch()
    {
        _timeTillSwitch = STATE_ACTIVE_TIME;
    }
}
