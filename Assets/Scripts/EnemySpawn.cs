﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EnemySpawn : MonoBehaviour
{
    //The amount of time remaining on the in-between wave timer that we pause at if the player is still outside
    //the combat zone.
    private const float PLAYER_OUTSIDE_ARENA_TIME_PAUSE = 5f;

    //How long is the wave over message displayed.
	private const float WAVE_OVER_DISPLAY_TIME = 3f;

    //How long is the you win message displayed.
    private const float _WIN_GAME_DISPLAY_TIME = 5f;


    //The grunt enemy game object.
    public GameObject gruntEnemyModel;

    //The light machine gune mech game object
    public GameObject lightMachineGunMechModel;

    //The wave to start on. Modifiable for testing purposes
    public int startingWave;

	//Publically accessible getter for the current wave.
	public int currentWave 
	{
		get 
		{
			return _currentWave;
		}
	}

    //The starting number of enemies for wave 1. This is also used as the base for later wave enemy number calculations.
    public int startingMaxNumEnemies;

    //The number of enemies added to each successive wave.
    public int enemyNumIncreasePerWave;

    //The time between each enemy spawning within a wave.
    public float timeBetweenSpawns;

    //The time between one wave ending and the next beginning.
    public float timeBetweenWaves;

    //The first waypoint enemies that spawn at spawn point 1 should travel to if gravity is on.
    public Waypoint groundSpawnPoint1StartingWaypoint;

    //The first waypoint enemies that spawn at spawn point 2 should travel to if gravity is on.
    public Waypoint groundSpawnPoint2StartingWaypoint;

	//The first waypoint enemies that spawn at spawn point 1 should travel to if gravity if off.
	public Waypoint airSpawnPoint1StartingWaypoint;

	//The first waypoint enemies that spawn at spawn point 2 should travel to if gravity if off.
	public Waypoint airSpawnPoint2StartingWaypoint;

	//Holds if the wave has ended or is in progress. Returns true if the wave is in progress and false if it has ended.
	public bool waveStatus
	{
		get
		{
			return !_isWaveEnded;
		}
	}

	//Holds how much time has elapsed since the end of the wave.
	public float betweenWaveTimer
	{
		get
		{
			return _betweenWaveTimer;
		}
	}

	//Holds the number of enemies left in the wave.
	public int remainingEnemyCount
	{
		get
		{
			return _currentWaveTotalEnemyCount - _numEnemiesPlayerHasKilled;
		}
	}

    //Holds which side the enemy spawned on.
    public int spawnSide
    {
        get
        {
            return _spawnSide;
        }
    }

	public Text waveOverText;

    //Text intended to inform the player when a game over occurs.
    public Text winGameText;

    //The current wave. Only modifiable within this script.
    private int _currentWave;

    //The number of enemies the player has to defeat to complete the wave.
	private int _currentWaveTotalEnemyCount;

    //The number of enemies that have spawned in the wave so far.
	private int _currentEnemyCount;

    //The number of enemies the player has killed.
	private int _numEnemiesPlayerHasKilled;

    //The maximum number of enemies allowed to spawn at any time. This is to prevent the player from being overwhelmed in later waves.
	private int _currentMaxNumEnemies = 5;

    //The current number of active enemies. Works with m_CurrentMaxNumberEnemies to prevent overwhelming the player with enemies.
	private int _currentNumActiveEnemies;

    //Has the wave finished?
	private bool _isWaveEnded;

    //The timer that tracks how much time has passed since the last enemy spawn.
	private float _inWaveTimer;

    //The timer that tracks how much time has passed since the last wave ended.
	private float _betweenWaveTimer;

    //One of the spawn possible spawn points for grunt enemies.
	private readonly Vector3 _gruntSpawnPoint1 = new Vector3(-10f, 48.535f, 86f); //(-21f, 48.535f, 86f) (-45f, 48.535f, 86f)

    //The other possible spawn point for grunt enemies.
	private readonly Vector3 _gruntSpawnPoint2 = new Vector3(-100f, 48.535f, 86f);

    //One of the possible spawn points for mech enemies.
	private readonly Vector3 _mechSpawnPoint1 = new Vector3(-26f, 48.535f, 86f);

    //The other possible spawn point for mech enemies.
	private readonly Vector3 _mechSpawnPoint2 = new Vector3(-84f, 48.535f, 86f);

    //Reference to the door that leads to the restocking area. We want this door to be locked when the wave is active.
	private HoriDoorManager _restockAreaDoor;

    //Do we need to lock or unlock the restock area door.
	private bool _restockAreaDoorSwitchNeeded;

    //Reference to the player in arena check script used to check if the player is in the arena during the between wave time.
	private PlayerInArenaCheck _playerInArenaCheck;

    //Reference to the Timer used to reset the state timer at the end of a wave.
	private GUITimer _GUITimer;

    //Reference to the Gravity Switch used to store and restore the gravity state at the end and beginning of the wave, respectively.
	private GravitySwitch _gravitySwitch;

    //Reference to the start game control script used to check if a game over state has occured.
	private StartGameControl _startGameControl;

    //Reference to the wave music control script used to switch which piece of music is playing.
	private WaveMusicControl _waveMusicControl;

    //Tracks how long the you won message has been displayed.
    private float _winGameTimer = 0.0f;

    //Has the win state been set?
    private bool _winSet = false;

    //Which side did the enemy spawn on?
    private int _spawnSide;


    // Use this for initialization
    void Start ()
	{
        _inWaveTimer = 0f;
        _restockAreaDoor = GameObject.FindGameObjectWithTag("RestockAreaDoor").GetComponentInChildren<HoriDoorManager>();
        _playerInArenaCheck = GameObject.FindGameObjectWithTag("ArenaTrigger").GetComponent<PlayerInArenaCheck>();
        _GUITimer = GameObject.FindGameObjectWithTag("GravitySwitch").GetComponent<GUITimer>();
        _gravitySwitch = GameObject.FindGameObjectWithTag("Player").GetComponent<GravitySwitch>();
        _startGameControl = GameObject.FindGameObjectWithTag("Player").GetComponent<StartGameControl>();
        _waveMusicControl = GameObject.FindGameObjectWithTag("Player").GetComponent<WaveMusicControl>();

        if (startingWave < 1)
        {
            StartWave(1);
        }
        else
        {
            StartWave(startingWave);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_winSet)
        {
            _winGameTimer += Time.deltaTime;
            if (_winGameTimer > _WIN_GAME_DISPLAY_TIME)
            {
                //Load the main menu (scene 0 in the build).
                SceneManager.LoadScene(0);
            }
        }

        if (!_startGameControl.isGameOver)
        {
            if (!_isWaveEnded)
            {
                if (_restockAreaDoorSwitchNeeded)
                {
                    _restockAreaDoor.LockDoor();
                    _restockAreaDoorSwitchNeeded = false;
                }

                //We spawn another enemy is the appropriate amount of time has passed since the last spawn, if the wave has enemies left to spawn, and if spawning
                //another enemy would not exceed the maximum amount of active enemies allowed.
                if (_inWaveTimer >= timeBetweenSpawns && _currentEnemyCount < _currentWaveTotalEnemyCount && _currentNumActiveEnemies < _currentMaxNumEnemies)
                {
                    Object newEnemy;
                    int randNum = Random.Range(0, 100);

                    //Spawn a mech if this an even wave and this the last enemy of the wave.
                    if (_currentWave % 2 == 0 && _currentWaveTotalEnemyCount - _currentEnemyCount == 1)
                    {
                        if (randNum < 50)
                        {
                            newEnemy = Instantiate(lightMachineGunMechModel, _mechSpawnPoint1, Quaternion.identity);
                        }
                        else
                        {
                            newEnemy = Instantiate(lightMachineGunMechModel, _mechSpawnPoint2, Quaternion.identity);
                        }
                    }
                    else
                    {
                        if (randNum < 50)
                        {
                            newEnemy = Instantiate(gruntEnemyModel, _gruntSpawnPoint1, Quaternion.identity);
                        }
                        else
                        {
                            newEnemy = Instantiate(gruntEnemyModel, _gruntSpawnPoint2, Quaternion.identity);
                        }
                    }

                    GameObject castedEnemy = (GameObject)newEnemy;
                    EnemyBehaviour enemyBehaviour = castedEnemy.GetComponent<EnemyBehaviour>();

					if (_gravitySwitch.gravityOn)
					{
						if (randNum >= 0 && randNum < 50)
						{
						    enemyBehaviour.SetCurrentWaypoint(groundSpawnPoint1StartingWaypoint, true);
                            _spawnSide = 1;
						}
						else
						{
						    enemyBehaviour.SetCurrentWaypoint(groundSpawnPoint2StartingWaypoint, true);
                            _spawnSide = 2;
                        }
					}
					else
					{
                        if (randNum >= 0 && randNum < 50)
                        {
                            enemyBehaviour.SetCurrentWaypoint(airSpawnPoint1StartingWaypoint, true);
                            _spawnSide = 1;
                        }
                        else
                        {
                            enemyBehaviour.SetCurrentWaypoint(airSpawnPoint2StartingWaypoint, true);
                            _spawnSide = 2;
                        }
                    }	

                    //This component has a tendency to deactivate itself when the game starts. No idea why, but we reactivate it.
                    vp_DamageHandler damageHandler = castedEnemy.GetComponent<vp_DamageHandler>();
                    damageHandler.enabled = true;
                    ++_currentEnemyCount;
                    ++_currentNumActiveEnemies;
                    _inWaveTimer = 0f;
                }
                else
                {
                    _inWaveTimer += Time.deltaTime;
                }

                if (_currentEnemyCount == _currentWaveTotalEnemyCount && _numEnemiesPlayerHasKilled == _currentWaveTotalEnemyCount)
                {
                    EndWave(_currentWave);
                }
            }
            else
            {
                if (_restockAreaDoorSwitchNeeded)
                {
                    _restockAreaDoor.UnlockDoor();
                    _restockAreaDoorSwitchNeeded = false;
                }

				if (_betweenWaveTimer > WAVE_OVER_DISPLAY_TIME)
				{
					waveOverText.text = "";
				}

                if (_betweenWaveTimer >= timeBetweenWaves)
                {
                    StartWave(++_currentWave);
                    _betweenWaveTimer = 0.0f;
                }
                else
                {
                    //24.99 seconds is used as opposed to 25 seconds flat because if 25 seconds is used, truncation makes the
                    //timer appear at 4 seconds, even though in actuality it's essentially 5 seconds.
					if (_betweenWaveTimer <= 54.98f || !_playerInArenaCheck.playerOutsideArena)
                    {
                        _betweenWaveTimer += Time.deltaTime;
                    }
                }
            }
        }
    }

    //Called from EnemyBehaviour when an enemy is killed to decrement the number of enemies in the arena and increment the number
    //of enemies the player has killed.
    public void DecrementCurrentEnemyCount()
    {
        --_currentNumActiveEnemies;
        ++_numEnemiesPlayerHasKilled;
    }

    //Calculates how many enemies need to spawn in the next wave and starts the wave.
    void StartWave(int wave)
    {
        _isWaveEnded = false;
        //Enemy counts increase on a linear scale based on the wave. For example, if there is a base starting count of 5 enemies in wave 1,
        //with an increase of 3 per wave, wave 8 would have 5 + 3 * 7 = 26 enemies to defeat.
        _currentWaveTotalEnemyCount = startingMaxNumEnemies + enemyNumIncreasePerWave * (wave - 1);
        _currentWave = wave;

        //If an even number wave has come up, we increase the maximum number of allowed active enemies.
        if (wave % 2 == 0)
        {
            ++_currentMaxNumEnemies;
        }

        _currentNumActiveEnemies = 0;
        _restockAreaDoorSwitchNeeded = true;

        if (wave != 1 && wave != startingWave)
        {
            _gravitySwitch.RestorePreviousGravityState();
			_GUITimer.switchNeeded = true;
        }

        _waveMusicControl.TransitionIntoWave();
    }

    //Marks the wave as over and resets key counting variables.
    void EndWave(int wave)
    {
        _isWaveEnded = true;
        _numEnemiesPlayerHasKilled = 0;
        _currentEnemyCount = 0;
        _restockAreaDoorSwitchNeeded = true;
        _gravitySwitch.StoreCurrentGravityState();
        _gravitySwitch.gravityOn = true;
        _GUITimer.switchNeeded = true;
        _GUITimer.ResetTimeTillSwitch();
        _waveMusicControl.TransitionOutOfWave();
		waveOverText.text = "Wave Over";

        if (wave >= 10)
        {
            winGameText.text = "You Win";
            GameObject.FindGameObjectWithTag("Player").GetComponent<StartGameControl>().isGameOver = true;
            _winSet = true;
        }
    }
}
