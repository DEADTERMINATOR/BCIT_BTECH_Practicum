﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using System.Collections;

public class PlayerInArenaCheck : MonoBehaviour
{
    //How long we display the game over message before we load the main menu.
    public const float MAX_GAME_OVER_TIMER = 5f;


    //The maximum amount of time the player can be outside the arena before they get a warning message.
    private const float _PRE_WARNING_OUTSIDE_ARENA_TIME = 54.98f;

    //The maximum amount of time the player can be outside the arena when there is only 5 seconds until the next wave.
	private const float _MAX_OUTSIDE_ARENA_TIME = 15f;


    //The transform of the player used in order to check the player's position and if they are in the main arena.
    public GameObject player;

    //Text intended to warn the player if they are outside the combat zone when a new wave is about to start.
    public Text warningText;

    //Text intended to inform the player when a game over occurs.
    public Text gameOverText;

	//Holds the status of the player with regards to whether they are in or out of the arena.
	//Enemy Spawn script needs this information to pause the in-between wave countdown timer if they are
	//still outside when there is only 5 seconds remaining. Return true if they are outside the arena,
	//false if they are not.
	public bool playerOutsideArena
	{
		get
		{
			return !_isInArena;
		}
	}


    //Reference to the enemy spawning script used to get the status of the wave and how much time is left
    //for the between wave break.
	private EnemySpawn _enemySpawn;

    //Is the player currently in the arena?
	private bool _isInArena;

    //Tracks how long the player is outside the combat zone when the new wave warning starts.
    //If the player is outside for too long, they fail.
	private float _outsideArenaTimer = 0.0f;

    //Tracks how long the game over message has been displayed.
	private float _gameOverTimer = 0.0f;

    //Has the game over state been set?
	private bool _gameOverSet = false;

    //Reference to the wave music control script used to switch which piece of music is playing.
	private WaveMusicControl _waveMusicControl;


    // Use this for initialization
    void Start ()
    {
        _enemySpawn = GameObject.FindGameObjectWithTag("Player").GetComponent<EnemySpawn>();
        _waveMusicControl = GameObject.FindGameObjectWithTag("Player").GetComponent<WaveMusicControl>();
        warningText.text = "";
	}
	
	// Update is called once per frame
	void Update ()
    {
        //We warn the player if they are outside the arena when there is only 5 seconds left until the next wave.
        //At this point, they have 15 seconds to get back in the arena, or they fail.
		if (!_enemySpawn.waveStatus && !_isInArena && _enemySpawn.betweenWaveTimer >= _PRE_WARNING_OUTSIDE_ARENA_TIME)
        {
            _outsideArenaTimer += Time.deltaTime;
            int secondsTruncated = (int)_outsideArenaTimer;
            warningText.text = "Return To The Arena: " + (_MAX_OUTSIDE_ARENA_TIME - secondsTruncated);
            if (_outsideArenaTimer >= _MAX_OUTSIDE_ARENA_TIME)
            {
                warningText.text = "";
                gameOverText.text = "Game Over";
                if (!_gameOverSet)
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<StartGameControl>().isGameOver = true;
                    _waveMusicControl.TransitionToOutOfArena();
                    _gameOverSet = true;
                }
            }
        }
        else
        {
            _outsideArenaTimer = 0.0f;
            warningText.text = "";
        }
        if (_gameOverSet)
        {
            _gameOverTimer += Time.deltaTime;
            if (_gameOverTimer > MAX_GAME_OVER_TIMER)
            {
                SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
            }
        }
    }

    //If this is called the player just entered the trigger volume that surrounds the combat zone.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            _isInArena = true;
        }
    }

    //If this is called the player just exited the trigger volume that surrounds the combat zone.
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            _isInArena = false;
        }
    }
}
