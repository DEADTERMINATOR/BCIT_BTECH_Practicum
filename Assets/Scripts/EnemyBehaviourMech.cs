﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBehaviourMech : EnemyBehaviour
{
	//The minimum and maximum amount of time the enemy will wait before moving if they are unable to hit the player.
	//We don't want the enemy to move the moment they lose sight of the player (e.g. the player ducks behind cover), as that is
	//not realistic.
	private const float _MINIMUM_TIME_BEFORE_MOVE = 7.5f;
	private const float _MAXIMUM_TIME_BEFORE_MOVE = 10.0f;

	//The base chance of an enemy not at a cover point moving after a shooting bout.
	private const int _BASE_CHANCE_OF_MOVING = 35;

	//The amount the chance of an enemy not at a cover point moving after a shooting bout increases everytime they do not move.
	private const int _INCREASE_CHANCE_OF_MOVING = 10;

	//Height offset for the bullets when the enemy fires their weapon.
	private Vector3 _BULLET_HEIGHT_OFFSET = new Vector3(0f, 2.25f, 0f);

	//The number of bullets fired in a single bout.
	private const int _NUM_TIMES_SHOOT_PER_BOUT = 1;


    //A struct that holds a waypoint and how far from the player that waypoint is.
    //Mechs are going to choose the waypoint closest to the player.
    struct ValidWaypoint
    {
        public Waypoint waypoint;
        public float distance;

        public ValidWaypoint(Waypoint waypoint, float distance)
        {
            this.waypoint = waypoint;
            this.distance = distance;
        }
    }


    //Enum of possible states the enemy could be in.
    public enum State { MOVING, SHOOTING, AT_WAYPOINT, NEEDS_NEW_WAYPOINT };

    //Which state the enemy is currently in.
    public State currentState;


	//The previous state the enemy was in. Used to know what state to return to when the enemy transfers to the SHOOTING state.
	private State _previousState;

	//The position the bullets will be spawned from for the right top gun.
	private Transform _rightTopShootingPosition;
	//The position the bullets will be spawned from for the right bottom gun.
	private Transform _rightBottomShootingPosition;
	//The position the bullets will be spawned from for the left top gun.
	private Transform _leftTopShootingPosition;
	//The position the bullets will be spawned from for the left bottom gun.
	private Transform _leftBottomShootingPosition;

    //The instantiated versions of the enemy's muzzle flashes.
    private GameObject _rightTopInstantiatedMuzzleFlash;
    private GameObject _rightBottomInstantiatedMuzzleFlash;
    private GameObject _leftTopInstantiatedMuzzleFlash;
    private GameObject _leftBottomInstantiatedMuzzleFlash;


    // Use this for initialization
    protected override void Start ()
    {
        base.Start();
		_rightTopShootingPosition = GameObject.FindGameObjectWithTag("MechRightTopShootingPosition").transform;
		_rightBottomShootingPosition = GameObject.FindGameObjectWithTag("MechRightBottomShootingPosition").transform;
		_leftTopShootingPosition = GameObject.FindGameObjectWithTag("MechLeftTopShootingPosition").transform;
		_leftBottomShootingPosition = GameObject.FindGameObjectWithTag("MechLeftBottomShootingPosition").transform;
	}

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            rigidBody.useGravity = true;

            if (currentState == State.MOVING)
            {
				anim.SetFloat("Speed", moveSpeed);

                if (!navMeshAgent.pathPending)
                {
                    if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                    {
                        if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                        {
                            currentState = State.AT_WAYPOINT;
                        }
                    }
                }
                else if (navMeshAgent.pathStatus == NavMeshPathStatus.PathInvalid || navMeshAgent.pathStatus == NavMeshPathStatus.PathPartial)
                {
                    SetNewNavPosition();
                }
            }
            else if (currentState == State.AT_WAYPOINT)
            {
				anim.SetFloat("Speed", 0f);

				int moveChance = Random.Range(0, 100);
				if (stayedAtWaypoint && moveChance <= chanceOfMoving)
				{
					currentState = State.NEEDS_NEW_WAYPOINT;
					chanceOfMoving = _BASE_CHANCE_OF_MOVING;
					stayedAtWaypoint = false;
				}
				else
				{
					transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), Time.deltaTime * 0.5f);
					currentState = State.SHOOTING;
					_previousState = State.AT_WAYPOINT;
					chanceOfMoving += _INCREASE_CHANCE_OF_MOVING;
                }
            }
            else if (currentState == State.SHOOTING)
            {
                anim.SetFloat("Speed", 0f);
                //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), Time.deltaTime);

                transform.LookAt(target);

                if (shootTime <= 0.0f && readyToFire)
				{
					currentState = _previousState;
					shootTime = Random.Range(MINIMUM_SHOOT_TIME, MAXIMUM_SHOOT_TIME);
                    stayedAtWaypoint = true;
                }
				else
				{
					if (readyToFire)
					{
						readyToFire = false;
						StartCoroutine(WaitBetweenShots());
					}
					shootTime -= Time.deltaTime;
                }

				RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(currentWaypoint, currentWaypoint.isCover);
				if (!enemySeesPlayerInfo.canSeeTarget)
				{
					currentTimeBeforeMove += Time.deltaTime;
					if (currentTimeBeforeMove >= timeBeforeMove)
					{
						currentState = State.NEEDS_NEW_WAYPOINT;
						timeBeforeMove = Random.Range(_MINIMUM_TIME_BEFORE_MOVE, _MAXIMUM_TIME_BEFORE_MOVE);
						currentTimeBeforeMove = 0.0f;
					}
				}
            }
            else if (currentState == State.NEEDS_NEW_WAYPOINT)
            {
                List<Waypoint> neighbours = currentWaypoint.neighbours; //A local copy of the current waypoint's neighbours.
                List<ValidWaypoint> possiblePoints = new List<ValidWaypoint>(); //Add any valid neighbours so we can randomly choose. Only regular points will be added. Mechs don't use cover, so we won't hog cover spots.

                foreach (Waypoint waypoint in neighbours)
                {
					if (!waypoint.isOccupied && !waypoint.isCover && waypoint.mask.value == (1 << 8)) //Make sure the waypoint isn't already occupied, that it isn't a cover waypoint, and that it is on the ground level (Mechs will always stay on the ground level).
                    {
						RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(waypoint, false);
						if (enemySeesPlayerInfo.canSeeTarget)
						{
							possiblePoints.Add(new ValidWaypoint(waypoint, enemySeesPlayerInfo.hit.distance));
						}
                    }
                }

                Waypoint newWaypoint = null;
                if (possiblePoints.Count != 0)
                {
                    float minDistance = float.MaxValue;
                    foreach (ValidWaypoint waypoint in possiblePoints)
                    {
                        if (waypoint.distance < minDistance)
                        {
                            minDistance = waypoint.distance;
                            newWaypoint = waypoint.waypoint;
                        }
                    }
                }
                else
                {
                    Collider[] waypointsNearPlayer = Physics.OverlapSphere(target.position, 10, (1 << 8)); //Bit shift to get the only the lower level ground waypoint layer (8).
                    if (waypointsNearPlayer.Length == 0)
                    {
                        //It's incredibly unlikely that this will ever trigger, but if by some chance the player is in a spot where a radius of 10 doesn't find a waypoint,
                        //we increase the radius by another 10.
                        int radiusIncrease = 10;
                        int increaseChances = 1;
                        while (waypointsNearPlayer.Length == 0 && increaseChances < 5)
                        {
                            waypointsNearPlayer = Physics.OverlapSphere(target.position, 10 + radiusIncrease, (1 << 8)); //Bit shift to get layer 8.
                            radiusIncrease += 10;
                            ++increaseChances;
                        }
                    }

                    int randomPoint;
                    if (waypointsNearPlayer.Length != 0)
                    {
                        //We have at least one waypoint near the player.
                        if (waypointsNearPlayer.Length > 1)
                        {
                            randomPoint = Random.Range(0, waypointsNearPlayer.Length);
                            newWaypoint = waypointsNearPlayer[randomPoint].gameObject.GetComponent<Waypoint>();
                        }
                        else
                        {
                            newWaypoint = waypointsNearPlayer[0].gameObject.GetComponent<Waypoint>();
                        }
                    }
                    else if (waypointsNearPlayer.Length == 0 && neighbours.Count != 0)
                    {
                        //Unbelievably, there are no waypoints near the player. So let's go to one of the current waypoint's neighbours and try everything again.
                        if (neighbours.Count > 1)
                        {
                            randomPoint = Random.Range(0, neighbours.Count);
                            newWaypoint = neighbours[randomPoint];
                        }
                        else
                        {
                            newWaypoint = neighbours[0];
                        }
                    }
                    else
                    {
                        //Some crazy BS is going on and apparently there no waypoints near the player and the current waypoint has no neighbours. Just keep shooting like a moron.
                        currentState = State.SHOOTING;
                    }
                }

                if (newWaypoint != null)
                {
					currentWaypoint.isOccupied = false;
                    currentWaypoint = newWaypoint;
                    newWaypoint.isOccupied = true;
                    currentState = State.MOVING;
                    SetNewNavPosition();
                    arrivedAtWaypoint = false;
                }
            }
        }
        else
        {
            deathTimer += Time.deltaTime;
            if (deathTimer > anim.GetCurrentAnimatorStateInfo(0).length)
            {
                rigidBody.isKinematic = false;
            }
        }
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        if (health <= 0.0f && !isDead)
        {
            isDead = true;
            rigidBody.isKinematic = true;
            int randomNum = Random.Range(0, 100);
            if (randomNum < 50)
            {
                //anim.SetBool("DeadFallBack", true);
                anim.Play("Fall_back");
            }
            else
            {
                //anim.SetBool("DeadFallForward", true);
                anim.Play("Fall_forward");
            }

            if (_rightTopInstantiatedMuzzleFlash != null)
            {
                Destroy(_rightTopInstantiatedMuzzleFlash);
            }
            if (_rightBottomInstantiatedMuzzleFlash != null)
            {
                Destroy(_rightBottomInstantiatedMuzzleFlash);
            }
            if (_leftTopInstantiatedMuzzleFlash != null)
            {
                Destroy(_leftTopInstantiatedMuzzleFlash);
            }
            if (_leftBottomInstantiatedMuzzleFlash != null)
            {
                Destroy(_leftBottomInstantiatedMuzzleFlash);
            }

            navMeshAgent.Stop();
            navMeshAgent.enabled = false;
            gameObject.GetComponent<NavMeshObstacle>().enabled = true;

            if (!batterySpawned)
            {
                GameObject battery = (GameObject)Instantiate(batteryPickup, transform.position + new Vector3(0f, 1.5f, 0f), Quaternion.identity);
                //int batteryAmount = Random.Range(50, 101);
                battery.GetComponent<BatteryPickup>().amountOfBatteryPower = 100;
                batterySpawned = true;
            }

            //Destroy the colliders so no more damage is taken, and the player can run through the body (makes it easier to pick up batteries at
            //the expense of realism).
            Destroy(GetComponent<BoxCollider>());
            Destroy(GetComponent<SphereCollider>());
        }
    }

    //Sets the correct position of the raycast bullets and the muzzle flashs and handles the raycast bullet collision checks.
    private IEnumerator WaitBetweenShots()
	{
        //Something has a tendency to destroy these shooting positions. It doesn't happen consistently or even in every playthrough, so I can't track what's doing it.
        //So just reassign them if they get destroyed.
        if (_rightTopShootingPosition == null || _rightBottomShootingPosition == null || _leftTopShootingPosition == null || _leftBottomShootingPosition == null)
        {
            _rightTopShootingPosition = GameObject.FindGameObjectWithTag("MechRightTopShootingPosition").transform;
            _rightBottomShootingPosition = GameObject.FindGameObjectWithTag("MechRightBottomShootingPosition").transform;
            _leftTopShootingPosition = GameObject.FindGameObjectWithTag("MechLeftTopShootingPosition").transform;
            _leftBottomShootingPosition = GameObject.FindGameObjectWithTag("MechLeftBottomShootingPosition").transform;
        }

        _rightTopInstantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, _rightTopShootingPosition.transform.position, _rightTopShootingPosition.transform.rotation);
        _rightBottomInstantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, _rightBottomShootingPosition.transform.position, _rightBottomShootingPosition.transform.rotation);
        _leftTopInstantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, _leftTopShootingPosition.transform.position, _leftTopShootingPosition.transform.rotation);
        _leftBottomInstantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, _leftBottomShootingPosition.transform.position, _leftBottomShootingPosition.transform.rotation);

        StartCoroutine(WaitBetweenFireSounds());
		for (int i = 0; i < _NUM_TIMES_SHOOT_PER_BOUT; ++i)
		{
			if (this != null)
			{
				Transform enemyShootTransform = new GameObject().transform;
                enemyShootTransform.position = transform.position + _BULLET_HEIGHT_OFFSET;
                enemyShootTransform.LookAt(GameObject.FindGameObjectWithTag("PlayerHead").transform);

				//transform.LookAt(target);

				RaycastHit[] hits = new RaycastHit[4];

				hits[0] = Shoot(enemyShootTransform.position, enemyShootTransform.transform.forward);
				hits[1] = Shoot(enemyShootTransform.position, enemyShootTransform.transform.forward);
				hits[2] = Shoot(enemyShootTransform.position, enemyShootTransform.transform.forward);
				hits[3] = Shoot(enemyShootTransform.position, enemyShootTransform.transform.forward);

				Destroy(enemyShootTransform.gameObject);
				foreach (RaycastHit hit in hits)
				{
					if (hit.collider.name == "PlayerSightCollider")
					{
						playerDamageHandler.AttemptDamage(CalculateEuclideanDistance(transform, target), (vp_Input.GetAxisRaw("Horizontal") != 0 || vp_Input.GetAxisRaw("Vertical") != 0) ? true : false, transform);
					}
				}

				//Debug.DrawRay(transform.position + _rightTopShootingPosition.localPosition + new Vector3(-1.5f, 0.65f, 1.15f) + _BULLET_HEIGHT_OFFSET, transform.forward * 150.0f, Color.green, 5.0f);
				//Debug.DrawRay(transform.position + _rightBottomShootingPosition.localPosition + new Vector3(-1.5f, 0.4f, 1.15f) + _BULLET_HEIGHT_OFFSET, transform.forward * 150.0f, Color.green, 5.0f);
				//Debug.DrawRay(transform.position + _leftTopShootingPosition.localPosition + new Vector3(-1.5f, 0.65f, -1.15f) + _BULLET_HEIGHT_OFFSET, transform.forward * 150.0f, Color.green, 5.0f);
				//Debug.DrawRay(transform.position + _leftBottomShootingPosition.localPosition + new Vector3(-1.5f, 0.4f, -1.15f) + _BULLET_HEIGHT_OFFSET, transform.forward * 150.0f, Color.green, 5.0f);
				//Debug.DrawRay(enemyShootTransform.position, transform.forward * 150f, Color.blue, 5.0f);

				yield return new WaitForSeconds(0.1f);
			}
		}

        //Same reason as above.
        if (_rightTopShootingPosition == null || _rightBottomShootingPosition == null || _leftTopShootingPosition == null || _leftBottomShootingPosition == null)
        {
            _rightTopShootingPosition = GameObject.FindGameObjectWithTag("MechRightTopShootingPosition").transform;
            _rightBottomShootingPosition = GameObject.FindGameObjectWithTag("MechRightBottomShootingPosition").transform;
            _leftTopShootingPosition = GameObject.FindGameObjectWithTag("MechLeftTopShootingPosition").transform;
            _leftBottomShootingPosition = GameObject.FindGameObjectWithTag("MechLeftBottomShootingPosition").transform;
        }

        Destroy(_rightTopInstantiatedMuzzleFlash);
        Destroy(_rightBottomInstantiatedMuzzleFlash);
        Destroy(_leftTopInstantiatedMuzzleFlash);
        Destroy(_leftBottomInstantiatedMuzzleFlash);

        readyToFire = true;
	}

    //Controls the playing of the weapon firing sounds.
    private IEnumerator WaitBetweenFireSounds()
	{
		for (int i = 0; i < _NUM_TIMES_SHOOT_PER_BOUT; ++i)
		{
			if (this != null)
			{
				enemyFiring.Play();
				yield return new WaitForSeconds(1f);
			}
		}
	}
}
