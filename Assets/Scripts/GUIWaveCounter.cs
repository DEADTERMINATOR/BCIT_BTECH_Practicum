﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIWaveCounter : MonoBehaviour
{
    //The text field for the wave count.
    public Text waveText;

    //The text field for the remaining enemy count.
    public Text enemyCountText;


    //The enemy spawner script
	private EnemySpawn _enemySpawn;


    // Use this for initialization
    void Start ()
    {
        _enemySpawn = GameObject.FindGameObjectWithTag("Player").GetComponent<EnemySpawn>();
        waveText.text = "Wave: " + _enemySpawn.startingWave;
		enemyCountText.text = "Enemies Remaining: " + _enemySpawn.remainingEnemyCount;
	}
	
	// Update is called once per frame
	void Update ()
    {
        waveText.text = "Wave: " + _enemySpawn.currentWave;
		if (!_enemySpawn.waveStatus)
        {
            enemyCountText.text = "Enemies Remaining: 0";
        }
        else
        {
            enemyCountText.text = "Enemies Remaining: " + _enemySpawn.remainingEnemyCount;
        }
    }
}
