﻿using UnityEngine;
using System.Collections;

//Adapted from work by Jereon Baert. 
public static class MortonOrder
{
	//The octree in the world resides in the negative x direction. If we offset by this value, then the lowest x-coordinate of the octree
	//will reside at 0, meaning we can treat all coordinates as unsigned values.
	public const int X_OFFSET = 101;


	//Encodes a 3-dimensional coordinate as a single dimension code that can be used for easy retrieval of a octree node.
	public static ulong MortonEncode(uint xPos, uint yPos, uint zPos)
	{
		ulong mortonCode = 0;
		mortonCode |= SplitBy3(xPos) | SplitBy3(yPos) << 1 | SplitBy3(zPos) << 2;
		return mortonCode;
	}

    //Decodes a Morton Code back into a 3-dimensional coordinate.
	public static Vector3 MortonDecode(ulong mortonCode)
	{
		Vector3 originalPosition = new Vector3(0, 0, 0);
		originalPosition.x = CompactBy3(mortonCode);
		originalPosition.y = CompactBy3(mortonCode >> 1);
		originalPosition.z = CompactBy3(mortonCode >> 2);
		return originalPosition;
	}

	//Interleaves one of the dimensions of a 3-dimensional point. Used to construct the Morton Code 1 dimension at a time.
	private static ulong SplitBy3(uint num)
	{
		ulong x = num & 0x1fffff;
		x = (x | x << 32) & 0x1f00000000ffff; //shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
		x = (x | x << 16) & 0x1f0000ff0000ff; //shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
		x = (x | x << 8) & 0x100f00f00f00f00f; //shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
		x = (x | x << 4) & 0x10c30c30c30c30c3; //shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
		x = (x | x << 2) & 0x1249249249249249;
		return x;
	}

    //Deinterleaves one of the dimensions of a 3-dimensional point. Used to deconstruct the Morton Code back to its original coordinates.
	private static ulong CompactBy3(ulong mortonCode)
	{
		ulong x = mortonCode & 0x1249249249249249;
		x = (x | x >> 2) & 0x10c30c30c30c30c3;
		x = (x | x >> 4) & 0x100f00f00f00f00f;
		x = (x | x >> 8) & 0x1f0000ff0000ff;
		x = (x | x >> 16) & 0x1f00000000ffff;
		x = (x | x >> 32) & 0x1fffff;
		return x;
	}
}
