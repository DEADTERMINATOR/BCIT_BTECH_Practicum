﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerInRestockRoom : MonoBehaviour
{
    //X-axis offsets for various item purchase texts to center the text.
	private const int _PISTOL_AMMO_X_OFFSET = 785;
	private const int _REAPER_SMG_X_OFFSET = 790;
	private const int _REAPER_AMMO_X_OFFSET = 735;
	private const int _ASSAULT_RIFLE_X_OFFSET = 790;
	private const int _ASSAULT_RIFLE_AMMO_X_OFFSET = 725;
	private const int _LASER_RIFLE_X_OFFSET = 805;
	private const int _LASER_RIFLE_AMMO_X_OFFSET = 755;
	private const int _HEALTH_PACK_X_OFFSET = 795;
	private const int _HEALING_X_OFFSET = 750;
	private const int _CANT_AFFORD_OFFSET = 650;
	private const int _CANT_BUY_HEALTH_OFFSET = 750;

    //The costs of the various items.
	private const int _REAPER_SMG_COST = 40;
	private const int _REAPER_AMMO_COST = 5;
	private const int _ASSAULT_RIFLE_COST = 70;
	private const int _ASSAULT_RIFLE_AMMO_COST = 15;
	private const int _LASER_RIFLE_COST = 100;
	private const int _LASER_RIFLE_AMMO_COST = 10;
	private const int _HEALTH_PACK_COST = 50;
	private const int _HEALING_COST = 10;


    //The transform of the player used in order to check the player's position and if they are in the restock.
    public GameObject player;

    //The text displaying the appropriate purchase offer depending on which crate is being looked at.
    public Text purchaseText;

    //The text displaying the cost of the item being offered.
    public Text costText;

    //Game objects that hold the various items that can be purchased.
    public GameObject reaperSMGPickup;
    public GameObject reaperAmmoPickup;
    public GameObject assaultRiflePickup;
    public GameObject assaultRifleAmmoPickup;
    public GameObject laserRiflePickup;
    public GameObject laserRifleAmmoPickup;

	//Publically accessible getters for whether the player owns the purchasable weapons.
	public bool reaperPurchased
	{
		get
		{
			return _reaperPurchased;
		}
	}
	public bool assaultRiflePurchased
	{
		get
		{
			return _assaultRiflePurchased;
		}
	}
	public bool laserRiflePurchased
	{
		get
		{
			return _laserRiflePurchased;
		}
	}

    //A sound effect to play when the player makes a purchase.
    public AudioSource purchaseSound;


    //Is the player currently in the restock room?
	private bool _isInRestockRoom;

    //Boolean variables tracking which weapons have been purchased so that ammo can be offered if the weapon is owned.
	private bool _reaperPurchased = false;
	private bool _assaultRiflePurchased = false;
	private bool _laserRiflePurchased = false;

    //Reference to the ammo display in order to correctly track the current weapon when the player buys a new weapon.
	private GUIAmmoDisplay _guiAmmoDisplay;

    //Used to check if the player even has enough battery power to make the purchase.
	private GUIHealthAndBatteryDisplay _guiHealthAndBatteryDisplay;


    // Use this for initialization
    void Start ()
    {
        purchaseText.text = "";
        costText.text = "";
        _guiAmmoDisplay = GameObject.FindGameObjectWithTag("AmmoInfo").GetComponent<GUIAmmoDisplay>();
        _guiHealthAndBatteryDisplay = GameObject.FindGameObjectWithTag("HealthAndBatteryInfo").GetComponent<GUIHealthAndBatteryDisplay>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if (_isInRestockRoom)
        {
            RaycastHit hit;
            Vector3 playerPosition = player.transform.position;
            playerPosition += new Vector3(0, 1.5f, 0); //Offset the player position by 2.5 units on the y-axis to bring the raycast to eye level.
            Quaternion cameraRotation = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>().rotation;

            Transform wherePlayerIsLooking = new GameObject().transform;
            wherePlayerIsLooking.position = playerPosition;
            wherePlayerIsLooking.rotation = cameraRotation;

            Physics.Raycast(wherePlayerIsLooking.position, wherePlayerIsLooking.forward, out hit, 2.0f);

			if (!ReferenceEquals(hit.collider, null))
			{
				if (hit.collider.gameObject.name == "ReaperAmmo")
				{
					if (!_reaperPurchased)
					{
						costText.text = "Cost: 40%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _REAPER_SMG_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase reaper SMG";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(reaperSMGPickup, player.transform.position, Quaternion.identity);
								Instantiate(reaperAmmoPickup, player.transform.position, Quaternion.identity);
								_reaperPurchased = true;
								_guiAmmoDisplay.currentWeapon = GUIAmmoDisplay.Weapon.REAPER;
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _REAPER_SMG_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for the reaper SMG";
						}
					}
					else
					{
						costText.text = "Cost: 5%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _REAPER_AMMO_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase reaper SMG ammo";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(reaperAmmoPickup, player.transform.position, Quaternion.identity);
								_guiAmmoDisplay.IncreaseReserves(GUIAmmoDisplay.Weapon.REAPER);
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _REAPER_AMMO_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for reaper SMG ammo";
						}
					}
				}
				else if (hit.collider.gameObject.name == "AssaultRifleAmmo")
				{
					if (!_assaultRiflePurchased)
					{
						costText.text = "Cost: 70%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _ASSAULT_RIFLE_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase assault rifle";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(assaultRiflePickup, player.transform.position, Quaternion.identity);
								Instantiate(assaultRifleAmmoPickup, player.transform.position, Quaternion.identity);
								_assaultRiflePurchased = true;
								_guiAmmoDisplay.currentWeapon = GUIAmmoDisplay.Weapon.ASSAULT_RIFLE;
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _ASSAULT_RIFLE_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for the assault rifle";
						}
					}
					else
					{
						costText.text = "Cost: 15%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _ASSAULT_RIFLE_AMMO_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase assault rifle ammo";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(assaultRifleAmmoPickup, player.transform.position, Quaternion.identity);
								_guiAmmoDisplay.IncreaseReserves(GUIAmmoDisplay.Weapon.ASSAULT_RIFLE);
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _ASSAULT_RIFLE_AMMO_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for assault rifle ammo";
						}
					}
				}
				else if (hit.collider.gameObject.name == "LaserRifleAmmo")
				{
					if (!_laserRiflePurchased)
					{
						costText.text = "Cost: 100%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _LASER_RIFLE_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase laser rifle";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(laserRiflePickup, player.transform.position, Quaternion.identity);
								for (int i = 0; i < 4; ++i)
								{
									Instantiate(laserRifleAmmoPickup, player.transform.position, Quaternion.identity);
								}
								_laserRiflePurchased = true;
								_guiAmmoDisplay.currentWeapon = GUIAmmoDisplay.Weapon.LASER_RIFLE;
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _LASER_RIFLE_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for the laser rifle";
						}
					}
					else
					{
						costText.text = "Cost: 10%";
						if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _LASER_RIFLE_AMMO_COST)
						{
							purchaseText.text = "Press 'E' or RB to purchase laser rifle ammo";
							if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
							{
								Instantiate(laserRifleAmmoPickup, player.transform.position, Quaternion.identity);
								_guiAmmoDisplay.IncreaseReserves(GUIAmmoDisplay.Weapon.LASER_RIFLE);
								_guiHealthAndBatteryDisplay.totalBatteryAmount -= _LASER_RIFLE_AMMO_COST;
								purchaseSound.Play();
							}
						}
						else
						{
							purchaseText.text = "You don't have enough energy for laser rifle ammo";
						}
					}
				}
				else if (hit.collider.gameObject.name == "PortableHealthPacks")
				{
					costText.text = "Cost: 50%";
					if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _HEALTH_PACK_COST)
					{
						purchaseText.text = "Press 'e' or RB to purchase a health pack";
						if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
						{
							_guiHealthAndBatteryDisplay.IncreaseHealthPackCount();
							_guiHealthAndBatteryDisplay.totalBatteryAmount -= _HEALTH_PACK_COST;
							purchaseSound.Play();
						}
					}
					else
					{
						purchaseText.text = "You don't have enough energy for a health pack";
					}
				}
				else if (hit.collider.gameObject.name == "Healing")
				{
					costText.text = "Cost: 10%";
					if (_guiHealthAndBatteryDisplay.totalBatteryAmount >= _HEALING_COST && _guiHealthAndBatteryDisplay.health != 200)
					{
						purchaseText.text = "Press 'e' or RB to purchase 10 points of health";
						if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Right Bumper"))
						{
							_guiHealthAndBatteryDisplay.IncreaseHealth();
							_guiHealthAndBatteryDisplay.totalBatteryAmount -= _HEALING_COST;
							purchaseSound.Play();
						}
					}
					else if (_guiHealthAndBatteryDisplay.health == 200)
					{
						purchaseText.text = "You can't purchase any more health.";
					}
					else
					{
						purchaseText.text = "You don't have enough energy for healing";
					}
				}
			}
			else
			{
				purchaseText.text = "";
				costText.text = "";
			}

            Destroy(wherePlayerIsLooking.gameObject);
        }
	}

    //If this is called the player just entered the trigger volume that surrounds the restock room.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            _isInRestockRoom = true;
        }
    }

    //If this is called the player just exited the trigger volume that surrounds the restock room.
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            _isInRestockRoom = false;
        }
    }
}
