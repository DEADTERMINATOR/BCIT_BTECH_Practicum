﻿using UnityEngine;
using System;
using System.Collections;

public class MortonCode : IComparable<MortonCode>
{
	//Getter for the Morton code.
	public ulong mortonCode
	{
		get
		{
			return _mortonCode;
		}
	}

	//Getter for the octree node associated with this Morton code.
	public OctreeNode associatedNode
	{
		get
		{
			return _associatedNode;
		}
	}


	//The actual Morton code.
	private ulong _mortonCode;

	//The node associated with this Morton code.
	private OctreeNode _associatedNode;


	public MortonCode(ulong mortonCode, ref OctreeNode associatedNode)
	{
		_mortonCode = mortonCode;
		_associatedNode = associatedNode;
	}

	public int CompareTo(MortonCode otherCode)
	{
		return otherCode.mortonCode.CompareTo(mortonCode);
	}
}
