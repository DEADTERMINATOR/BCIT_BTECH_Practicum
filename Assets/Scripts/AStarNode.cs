﻿using UnityEngine;
using System.Collections;

public class AStarNode 
{
	//The current node.
	public OctreeNode node;

	//The most efficient node to come from to reach this node.
	public AStarNode cameFrom;

	//The cost of going from the starting node to the node we are currently at.
	public float gScore;

	//The cost of going from this node to the end node.
	public float fScore;


	public AStarNode(OctreeNode node)
	{
		this.node = node;
		cameFrom = null;
	}

	public override bool Equals(object obj)
	{
		AStarNode castedObj = (AStarNode)obj;
		return ReferenceEquals(node, castedObj.node);
	}
}
