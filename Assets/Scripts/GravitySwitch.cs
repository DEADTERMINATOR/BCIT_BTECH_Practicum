﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GravitySwitch : MonoBehaviour
{
    //Holds whether gravity is currently on.
    public bool gravityOn;
    
    //Reference to the timer that tracks when switches should occur.
    public GUITimer timer;


    //Reference to the player controller. Used to change movement functionality on gravitational state switch.
	private vp_FPController _playerController;

    //What was the gravitational state on the conclusion of the previous wave.
	private bool _lastWaveGravityState = true;

    //Normally, we keep flipping between the two states, but this allows us to break the pattern as needed.
    //e.g. Restoring gravity at the end of a wave or restoring the previous state at the start of the next.
    private bool _switchByOverride = false;
    

	// Use this for initialization
	void Start ()
	{
	    _playerController = GetComponent<vp_FPController>();
        if (!gravityOn)
        {
            _playerController.MotorFreeFly = true;
            _playerController.PhysicsGravityModifier = 0.0f;
            //playerController.MotorJumpForce = 0.09f;
            //playerController.MotorJumpForceDamping = 0.0067f;
        }
        else
        {
            _playerController.MotorFreeFly = false;
            _playerController.PhysicsGravityModifier = 0.2f;
            //playerController.MotorJumpForce = 0.18f;
            //playerController.MotorJumpForceHold = 0.0067f;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (timer.switchNeeded)
	    {
            if (!_switchByOverride)
            {
                gravityOn = !gravityOn;
            }
            else
            {
                _switchByOverride = false;
            }

            if (!gravityOn)
	        {
	            _playerController.MotorFreeFly = true;
	            _playerController.PhysicsGravityModifier = 0.0f;
                //playerController.MotorJumpForce = 0.09f;
                //playerController.MotorJumpForceDamping = 0.0067f;
            }
	        else
	        {
	            _playerController.MotorFreeFly = false;
	            _playerController.PhysicsGravityModifier = 0.2f;
                //playerController.MotorJumpForce = 0.09f;
                //playerController.MotorJumpForceHold = 0.0067f;
            }

			timer.switchNeeded = false;

            //Make sure all the grunt enemies know to update their behaviours based on the new gravity situation.
            foreach (EnemyBehaviour enemy in EnemyDestructionManager.enemies)
            {
                if (enemy is EnemyBehaviourGrunt)
                {
                    EnemyBehaviourGrunt grunt = enemy as EnemyBehaviourGrunt;
                    grunt.gravitySwitchNeeded = true;
                }
            }
	    }
	}

    //Stores the gravity state at the end of a wave.
    public void StoreCurrentGravityState()
    {
        _lastWaveGravityState = gravityOn;
        _switchByOverride = true;
    }

    //Restores the gravity state at the beginning of the next wave.
    public void RestorePreviousGravityState()
    {
        gravityOn = _lastWaveGravityState;
        _switchByOverride = true;
    }
}
