﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

//A wrapper class for the load scene method as button clicks cannot call static methods in Unity.
public class LoadSceneOnClick : MonoBehaviour
{
    public void LoadSceneWrapper(int sceneIndex)
    {
        LoadScene.LoadByIndex(1);
    }
}
