﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InformationMenu : MonoBehaviour
{
    public GameObject howToPlayLabel;
    public GameObject howToPlayText;

    public GameObject storyLabel;
    public GameObject storyText;

    public GameObject controlsLabel;
    public GameObject controlsPanel;
    public GameObject keyboardControlsText;
    public GameObject controllerControlsText;

    public GameObject continueLeft;
    public GameObject continueRight;
    public GameObject continueBoth;


    private int _currentPage = 1;

    private float _previousLeftAnalogXAxis;

    private bool _leftAnalogRested = true;


    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _previousLeftAnalogXAxis = Input.GetAxis("Left Analog X");
        if (!_leftAnalogRested)
        {
            LeftAnalogStickAtRest();
        }

        if ((Input.GetKeyDown(KeyCode.LeftArrow) || (Input.GetAxis("Left Analog X") < -0.5 && _leftAnalogRested)) && _currentPage > 1)
        {
            _leftAnalogRested = false;
            --_currentPage;
        }
        else if ((Input.GetKeyDown(KeyCode.RightArrow) || (Input.GetAxis("Left Analog X") > 0.5 && _leftAnalogRested)) && _currentPage < 3)
        {
            _leftAnalogRested = false;
            ++_currentPage;
        }

        switch (_currentPage)
        {
            case 1:
                howToPlayLabel.SetActive(true);
                howToPlayText.SetActive(true);

                storyLabel.SetActive(false);
                storyText.SetActive(false);

                controlsLabel.SetActive(false);
                controlsPanel.SetActive(false);
                keyboardControlsText.SetActive(false);
                controllerControlsText.SetActive(false);

                continueLeft.SetActive(false);
                continueRight.SetActive(true);
                continueBoth.SetActive(false);

                break;
            case 2:
                howToPlayLabel.SetActive(false);
                howToPlayText.SetActive(false);

                storyLabel.SetActive(true);
                storyText.SetActive(true);

                controlsLabel.SetActive(false);
                controlsPanel.SetActive(false);
                keyboardControlsText.SetActive(false);
                controllerControlsText.SetActive(false);

                continueLeft.SetActive(false);
                continueRight.SetActive(false);
                continueBoth.SetActive(true);

                break;
            case 3:
                howToPlayLabel.SetActive(false);
                howToPlayText.SetActive(false);

                storyLabel.SetActive(false);
                storyText.SetActive(false);

                controlsLabel.SetActive(true);
                controlsPanel.SetActive(true);
                keyboardControlsText.SetActive(true);
                controllerControlsText.SetActive(true);

                continueLeft.SetActive(true);
                continueRight.SetActive(false);
                continueBoth.SetActive(false);

                break;
            default:
                howToPlayLabel.SetActive(false);
                howToPlayText.SetActive(false);

                storyLabel.SetActive(false);
                storyText.SetActive(false);

                controlsLabel.SetActive(false);
                controlsPanel.SetActive(false);
                keyboardControlsText.SetActive(false);
                controllerControlsText.SetActive(false);

                continueLeft.SetActive(false);
                continueRight.SetActive(false);
                continueBoth.SetActive(false);

                break;
        }
    }

    private void LeftAnalogStickAtRest()
    {
        if (_previousLeftAnalogXAxis <= 0.05f && _previousLeftAnalogXAxis >= -0.05f)
        {
            _leftAnalogRested = true;
        }
    }
}
