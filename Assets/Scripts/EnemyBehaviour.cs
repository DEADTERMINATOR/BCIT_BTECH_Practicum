﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour
{
    /*
     * None of the following accuracy modifiers combine, the player will receive whichever applicable modifier would benefit them greatest.
     * e.g. a crouching player who is also moving does not receive both the crouching and slow moving modifiers, only the crouching modifier.
    */

    //How much the likelihood of the enemy hitting the player with their bullets is modified if the player is crouching.
    public const float PLAYER_CROUCHING_ACCURACY_MODIFIER = 0.75f;

    //How much the likelihood of the enemy hitting the player with their bullets is modified if the player is moving around.
    public const float PLAYER_MOVING_ACCURACY_MODIFIER = 0.6f;

    //How much the likelihood of the enemy hitting the player with their bullets is modified if the player is moving around slowly.
    //e.g. the player is moving while zoomed in.
    //public const float PLAYER_SLOW_MOVING_ACCURACY_MODIFIER = 0.8f;


    //The minimum and maximum amount of time the enemy will shoot when they enter the shooting state.
    protected const float MINIMUM_SHOOT_TIME = 1.5f;
    protected const float MAXIMUM_SHOOT_TIME = 2.5f;


    //The base move speed for enemies.
    public float moveSpeed;

    //The base jumping speed for enemies. In this context, this will act as the speed at which they
    //ascend or descend while in no-gravity space.
    public float jumpSpeed;

    //The amount of health the enemy has.
    public float health;

	//Used to mark if the enemy is dead. Enemies remain in the game world for a period of time after death.
	//This prevents updates from being performed on the enemy after they have been killed. It is also used
	//by the enemy destruction manager to know when to begin tracking how long the enemy has been dead.
	public bool isDead = false;

	//Holds how long the enemy has been dead for. We want to remove enemies that have been dead for 60 seconds or more from the arena.
	public float howLongDead = 0.0f;

	//The game object that holds the muzzle flash effect used when the enemy fires their weapon.
	public GameObject muzzleFlash;

    //The game object that holds the batter pickup that may spawn on an enemy's death.
	public GameObject batteryPickup;


    //The active target of the enemy. In this context, it will be the player.
    protected Transform target;

    //The enemy spawning script. This script reports the death of an enemy to the spawning script.
    protected EnemySpawn enemySpawn;

    //Used to manage animation states for the enemy characters.
    protected Animator anim;

    //Reference to the rigidbody used to control when the kinematic functionality of the enemy is turned on or off.
    //Used to play animations without the inteference of physics (as that causes unintended, but humorous, side effects),
    //and to control when the enemy is affected by gravity.
    protected Rigidbody rigidBody;

    //Reference to the gravity switch script so the enemy can be aware of the current gravitations state and act accordingly.
    protected GravitySwitch gravitySwitch;

    //Used to track how long as passed since the enemy died and ensure enough time passed since death to allow the death animation
    //to play before turning off kinematic movement.
    protected float deathTimer = 0.0f;

    //The current waypoint the enemy is either travelling to or currently occupying.
    public Waypoint currentWaypoint;

    //Has the enemy arrived at the current waypoint.
    protected bool arrivedAtWaypoint = false;

    //Reference to the enemy's nav mesh agent used to control their movement on the nav mesh when gravity is turned on.
    protected NavMeshAgent navMeshAgent;

    //A random amount of time that the enemy will shoot for when they enter a shooting state.
    protected float shootTime = 0.0f;

    //A reference to the player damage handler script so enemies can attempt to damage the player.
    protected PlayerDamageHandler playerDamageHandler;

	//A reference to the enemy destruction manager which removes enemies from the arena after they have been dead for some time.
	protected EnemyDestructionManager enemyDestroyer;

	//Allows the program to continue executing without waiting for the firing bout coroutine to finish by simply checking if this is set
	//which is set at the end of the coroutine.
	protected bool readyToFire = true;

	//The audio source for firing the enemy's weapon
	protected AudioSource enemyFiring;

	//The amount of time the enemy will wait (a value between the min and max const values defined individually for both enemy types) before moving
	//if they can't see the player.
	protected float timeBeforeMove;

	//How long has the enemy not been able to see the player.
	protected float currentTimeBeforeMove = 0.0f;

	//The chance that an enemy will move to another waypoint if they are at a waypoint that is not cover.
	//This increases each time they choose not to move, increasing the liklihood they will move on the next check.
	//This value is ignored if the enemy needs to move to see the player.
	protected int chanceOfMoving = 50;

	//Holds whether the enemy has stayed at the current waypoint for at least one shooting bout.
	protected bool stayedAtWaypoint = false;

    //Reference to the enemy's damage handler. 
	protected vp_DamageHandler vpDamageHandler;

    //Did the enemy already spawn their battery (if they were going to spawn one)?
    protected bool batterySpawned = false;


    // Use this for initialization
    protected virtual void Start ()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        enemySpawn = target.GetComponent<EnemySpawn>();
		if (enemySpawn.currentWave > 1)
		{
            if (this is EnemyBehaviourGrunt)
            {
                for (int i = 1; i < enemySpawn.currentWave; ++i)
                {
                    health *= 1.2f;
                }
            }
            else
            {
                for (int i = 1; i < enemySpawn.currentWave / 2; ++i)
                {
                    health *= 1.2f;
                }
            }
			vpDamageHandler = GetComponent<vp_DamageHandler>();
			vpDamageHandler.MaxHealth = health;
            Debug.Log("Wave " + enemySpawn.currentWave + ": Enemy Health: " + health);
		}
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        gravitySwitch = GameObject.FindGameObjectWithTag("Player").GetComponent<GravitySwitch>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        shootTime = Random.Range(MINIMUM_SHOOT_TIME, MAXIMUM_SHOOT_TIME);
        playerDamageHandler = target.gameObject.GetComponent<PlayerDamageHandler>();
		enemyDestroyer = target.gameObject.GetComponent<EnemyDestructionManager>();
		enemyDestroyer.AddEnemyForTracking(this);
		enemyFiring = GetComponents<AudioSource>()[0];
    }

    //Handles taking damage and enemy death functionality common to both enemy types.
    public virtual void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0.0f && !isDead)
        {
            if (gravitySwitch.gravityOn)
            {
                rigidBody.isKinematic = true;
            }
            enemySpawn.DecrementCurrentEnemyCount();
			currentWaypoint.isOccupied = false;
        }
    }

    //Handles waypoint setting functionality common to both enemy types.
	public virtual void SetCurrentWaypoint(Waypoint waypoint, bool justSpawned)
    {
        currentWaypoint = waypoint;
        currentWaypoint.isOccupied = true;
        if (navMeshAgent == null)
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }
        if (gravitySwitch == null)
        {
            gravitySwitch = GameObject.FindGameObjectWithTag("Player").GetComponent<GravitySwitch>();
        }
    }

    protected void SetNewNavPosition()
    {
        navMeshAgent.SetDestination(currentWaypoint.transform.position);
    }

    //Checks if the enemy can see the player from a waypoint the enemy is considering.
    protected RaycastInfo CanEnemySeePlayer(Waypoint waypoint, bool isCover)
    {
        RaycastHit hit;

        Vector3 waypointPosition = waypoint.transform.position; //The position of the waypoint being consider.
        if (isCover)
        {
            //If the waypoint is a cover waypoint, we need to offset the position to eye level. Otherwise, we just hit the cover in front of the enemy.
            waypointPosition += new Vector3(0, 2.5f, 0);
        }
        Quaternion enemyRotation = transform.rotation; //The rotation of the enemy.

        Transform enemyLookingAtPlayer = new GameObject().transform; //Create a temporary transform that hold the position of the waypoint and the rotation of the enemy. Simulating as if the enemy was already at that waypoint.
        enemyLookingAtPlayer.position = waypointPosition; //Set the temporary transform to the above variables.
        enemyLookingAtPlayer.rotation = enemyRotation;

        Transform targetEyeLevel = new GameObject().transform; //Create a temporary transform that holds the position of the player, but modified to be at eye level.
        targetEyeLevel.position = target.position + new Vector3(0, 1.75f, 0); // Offset to look at the head of the player, as opposed to the foot level the position reports.
        enemyLookingAtPlayer.LookAt(targetEyeLevel); //Simulate as if the enemy is looking at the player from the new waypoint.

        Physics.Raycast(enemyLookingAtPlayer.position, enemyLookingAtPlayer.forward, out hit, Mathf.Infinity, (1 << 12) | (1 << 13)); //Bit shift to get the geometry and player sight layers.
        //Debug.DrawRay(enemyLookingAtPlayer.position, enemyLookingAtPlayer.forward * 150.0f, Color.red, 25.0f);

        //Destroy the temporary game objects.
        Destroy(enemyLookingAtPlayer.gameObject);
        Destroy(targetEyeLevel.gameObject);

        if (hit.collider.name == "PlayerSightCollider") //We look for the trigger collider that defines what we want to be the visible part of the player's body.
        {
			return new RaycastInfo(hit, true);
        }
		return new RaycastInfo(hit, false);
    }

    //Checks if the player can see the cover waypoint from where they are now.
    protected RaycastInfo CanPlayerSeeCoverPoint(Waypoint coverWaypoint)
    {
        RaycastHit hit;

        Vector3 playerPosition = target.position; //The player's position.
        playerPosition += new Vector3(0, 2.5f, 0); //We offset the position of the raycast by 2.5 units on the y-axis to account for the fact that the transform of the player is reported on the ground. We want roughly eye level.
        Quaternion playerRotation = target.rotation; //The player's rotation.

        Transform playerLookingAtWaypoint = new GameObject().transform; //Need to create a temporary transform so as not to modify the actual player's position and rotation.
        playerLookingAtWaypoint.position = playerPosition; //Set the temporary transform to the above variables.
        playerLookingAtWaypoint.rotation = playerRotation;
        playerLookingAtWaypoint.LookAt(coverWaypoint.transform); //Simulate the player looking at the waypoint so we can test if they would be able to see the waypoint.

        Physics.Raycast(playerLookingAtWaypoint.position, playerLookingAtWaypoint.forward, out hit, Mathf.Infinity, (1 << 8) | (1 << 11) | (1 << 12)); //Bit shift to get the two ground waypoint and geometry layers.
        //Debug.DrawRay(playerLookingAtWaypoint.position, playerLookingAtWaypoint.forward * 150.0f, Color.blue, 25.0f);

        Destroy(playerLookingAtWaypoint.gameObject); //Destroy the temporary game object.
        if (hit.collider.name != coverWaypoint.gameObject.name)
        {
			return new RaycastInfo(hit, false);
        }
		return new RaycastInfo(hit, true);
    }

    //Perform the raycast for the shot.
    protected RaycastHit Shoot(Vector3 shootPosition, Vector3 forward)
    {
        RaycastHit hit;
        Physics.Raycast(shootPosition, forward, out hit, Mathf.Infinity, (1 << 12) | (1 << 13));
        return hit;
    }

    //Calculates the euclidean distance between two points.
	protected float CalculateEuclideanDistance(Transform startingTransform, Transform targetTransform)
	{
		return Mathf.Sqrt(Mathf.Pow(startingTransform.position.x - targetTransform.position.x, 2) + 
			Mathf.Pow(startingTransform.position.y - targetTransform.position.y, 2) + Mathf.Pow(startingTransform.position.z - targetTransform.position.z, 2));
	}
}
