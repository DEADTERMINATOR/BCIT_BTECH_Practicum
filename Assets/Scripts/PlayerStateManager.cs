﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerStateManager : MonoBehaviour
{
    //Various constant values for weapon and player positioning based on the current state.
	private const float _MOVE_STANDING = 0.18f;
	private const float _MOVE_CROUCHING = 0.09f;
	private const float _MOVE_ZOOMING = 0.05f;
    private const float _MOVE_DEAD = 0;

	private Vector2 _SENSITIVITY_NOT_ZOOMING;
	private Vector2 _SENSITIVITY_ZOOMING;

	private Vector3 _CAMERA_POSITION_STANDING = new Vector3(0, 1.75f, 0.1f);
	private Vector3 _CAMERA_POSITION_CROUCHING = new Vector3(0, 1.0f, 0.1f);
    private Vector3 _CAMERA_POSITION_DEAD = new Vector3(0, 0.75f, 0.1f);

	private const int _CAMERA_FIELD_OF_VIEW_NOT_ZOOMING = 60;
	private const int _CAMERA_FIELD_OF_VIEW_ZOOMING = 30;

	private Vector3 _PISTOL_POSITION_STANDING = new Vector3(0.15f, -0.45f, -0.15f);
	private Vector3 _PISTOL_POSITION_CROUCHING = new Vector3(0.15f, -0.3f, -0.15f);
	private Vector3 _PISTOL_POSITION_ZOOMING = new Vector3(0.15f, -0.45f, -0.5f);
    private Vector3 _PISTOL_POSITION_VR_ZOOMING = new Vector3(0.05f, -0.4f, -0.5f);
    private Vector3 _PISTOL_POSITION_RELOADING = new Vector3(0.15f, -0.9f, -0.15f);

	private Vector3 _REAPER_POSITION_STANDING = new Vector3(0.25f, -0.25f, 0.75f);
    private Vector3 _REAPER_POSITION_VR_STANDING = new Vector3(0.25f, -0.25f, 0.3f);
    private Vector3 _REAPER_POSITION_CROUCHING = new Vector3(0.25f, -0.35f, 0.75f);
	private Vector3 _REAPER_POSITION_ZOOMING = new Vector3(0.25f, -0.25f, 0.25f);
    private Vector3 _REAPER_POSITION_VR_ZOOMING = new Vector3(0, -0.25f, 0.25f);
    private Vector3 _REAPER_POSITION_RELOADING = new Vector3(0.25f, -0.9f, 0.75f);

	private Vector3 _ASSAULT_RIFLE_POSITION_STANDING = new Vector3(0.15f, -0.15f, 0.75f);
    private Vector3 _ASSAULT_RIFLE_POSITION_VR_STANDING = new Vector3(0.15f, -0.15f, 0.85f);
    private Vector3 _ASSAULT_RIFLE_POSITION_CROUCHING = new Vector3(0.15f, -0.25f, 0.75f);
	private Vector3 _ASSAULT_RIFLE_POSITION_ZOOMING = new Vector3(0.15f, -0.25f, 0.25f);
    private Vector3 _ASSAULT_RIFLE_POSITION_VR_ZOOMING = new Vector3(0f, -0.2f, 0.25f);
    private Vector3 _ASSAULT_RIFLE_POSITION_RELOADING = new Vector3(0.15f, -0.9f, 0.75f);

	private Vector3 _LASER_RIFLE_POSITION_STANDING = new Vector3(0.15f, -0.25f, 1.0f);
    private Vector3 _LASER_RIFLE_POSITION_VR_STANDING = new Vector3(0.15f, -0.25f, 1.25f);
    private Vector3 _LASER_RIFLE_POSITION_CROUCHING = new Vector3(0.15f, -0.35f, 1.0f);
	private Vector3 _LASER_RIFLE_POSITION_ZOOMING = new Vector3(0.15f, -0.35f, 0);
    private Vector3 _LASER_RIFLE_POSITION_VR_ZOOMING = new Vector3(0f, -0.3f, 0);
    private Vector3 _LASER_RIFLE_POSITION_RELOADING = new Vector3(0.15f, -0.9f, 0.75f);

	private Vector3 _PISTOL_MUZZLE_FLASH_POSITION_STANDING = new Vector3(0.2f, 0.1f, 1.1f);
	private Vector3 _PISTOL_MUZZLE_FLASH_POSITION_CROUCHING = new Vector3(0.2f, 0, 1.1f);

	private Vector3 _REAPER_MUZZLE_FLASH_POSITION_STANDING = new Vector3(0.2f, -0.1f, 1.2f);
	private Vector3 _REAPER_MUZZLE_FLASH_POSITION_CROUCHING = new Vector3(0.2f, -0.2f, 1.2f);

	private Vector3 _ASSAULT_RIFLE_MUZZLE_FLASH_POSITION_STANDING = new Vector3(0.2f, -0.1f, 1.2f);
	private Vector3 _ASSAULT_RIFLE_MUZZLE_FLASH_POSITION_CROUCHING = new Vector3(0.2f, -0.2f, 1.2f);

    //private Vector3 LASER_RIFLE_MUZZLE_FLASH_POSITION_STANDING;
    //private Vector3 LASER_RIFLE_MUZZLE_FLASH_POSITION_CROUCHING;

	private const int _PISTOL_SPREAD_NOT_ZOOMING = 1;
	private const int _PISTOL_SPREAD_ZOOMING = 0;

	private const int _REAPER_SPREAD_NOT_ZOOMING = 5;
	private const int _REAPER_SPREAD_ZOOMING = 3;

	private const int _ASSAULT_RIFLE_SPREAD_NOT_ZOOMING = 3;
	private const int _ASSAULT_RIFLE_SPREAD_ZOOMING = 1;


    //Enum of the possible states the player can be in.
    public enum PlayerState { STANDING, CROUCHING, ZOOMING, CROUCHZOOMING, DEAD };


    //The player game object. Used to get the player controller and the player input.
    public GameObject player;

    //The camera game object. Used to get the camera controller.
    public GameObject gameCamera;

    //The game objects for each of the weapons. Used to get each weapons' weapon script and shooter script.
    public GameObject pistol;
    public GameObject reaper;
    public GameObject assaultRifle;
    public GameObject laserRifle;

    //The current state of the player;
    public PlayerState currentState;

	//Publically accessible getter/setter for whether the player is reloading.
	public bool reloading
	{
		get
		{
			return _isReloading;
		}

		set 
		{
			PrepareReload();
		}
	}


    //Used to change movement speed of the player based on the player's current stance.
	private vp_FPController _playerController;

    //Used to change the sensitivity of mouse control based on whether the player is zoomed in or not.
	private vp_FPInput _playerInput;

    //Used to change the height of the camera to reflect whether the player is standing or crouched.
	private vp_FPCamera _cameraController;

    //Used to change the positioning of the corresponding weapon on the screen based on the player's current stance.
	private vp_FPWeapon _pistolWeapon;
	private vp_FPWeapon _reaperWeapon;
	private vp_FPWeapon _assaultRifleWeapon;
	private vp_FPWeapon _laserRifleWeapon;

    //Used to change the muzzle flash position of the corresponding weapon on the screen baed on the player's current stance.
    //Also used to change the spread factor of the bullets fired based on whether the player is zoomed in or not.
	private vp_FPWeaponShooter _pistolShooter;
	private vp_FPWeaponShooter _reaperShooter;
	private vp_FPWeaponShooter _assaultRifleShooter;
	private vp_FPWeaponShooter _laserRifleShooter;

    //Is the player currently reloading.
	private bool _isReloading = false;

    //Did the state just change. If so, we need to perform the appropriate changes.
	private bool _stateChangeRequired = false;

    //Reference to the player event handler used to check ammo status.
	private vp_PlayerEventHandler _eventHandler;

    //The total reload time for the currently equipped weapon.
	private float _reloadTime;

    //How much time has elapsed since the reload began.
	private float _currentReloadTime;

    //Used to allow the player to remain zoomed when zoom is activated on the controller. Otherwise, it's hold to zoom which is awkward on the right thumbstick.
	private bool _isZoomed = false;

	//Has the right stick been released. Clicking the right stick both zooms in and zooms out. This is used to force the player to release the stick in between,
	//so zooming is not activated then immediately deactivated.
	private bool _rightStickReleased = true;

    //Reference to the gravity switch so we know if we need to deactivate crouching when gravity turns off.
    private GravitySwitch _gravitySwitch;


	// Use this for initialization
	void Start ()
    {
        _SENSITIVITY_NOT_ZOOMING = new Vector2(PlayerPrefs.GetFloat("AimingSensitivity") * 20 + 0.1f, PlayerPrefs.GetFloat("AimingSensitivity") * 20 + 0.1f);
        _SENSITIVITY_ZOOMING = new Vector2(PlayerPrefs.GetFloat("AimingSensitivity") * 1 + 0.1f, PlayerPrefs.GetFloat("AimingSensitivity") * 1 + 0.1f); //Zoomed sensitivity should be 5% of base sensitivity.

        _playerController = player.GetComponent<vp_FPController>();
        _playerInput = player.GetComponent<vp_FPInput>();
        _cameraController = gameCamera.GetComponent<vp_FPCamera>();

        _pistolWeapon = pistol.GetComponent<vp_FPWeapon>();
        _reaperWeapon = reaper.GetComponent<vp_FPWeapon>();
        _assaultRifleWeapon = assaultRifle.GetComponent<vp_FPWeapon>();
        _laserRifleWeapon = laserRifle.GetComponent<vp_FPWeapon>();

        _pistolShooter = pistol.GetComponent<vp_FPWeaponShooter>();
        _reaperShooter = reaper.GetComponent<vp_FPWeaponShooter>();
        _assaultRifleShooter = assaultRifle.GetComponent<vp_FPWeaponShooter>();
        _laserRifleShooter = laserRifle.GetComponent<vp_FPWeaponShooter>();

        _eventHandler = _pistolWeapon.Player; //We use the pistolWeapon just to get a reference to the eventHandler. We don't need one for each weapon.

        currentState = PlayerState.STANDING;
        _stateChangeRequired = true;
        _gravitySwitch = GameObject.FindGameObjectWithTag("Player").GetComponent<GravitySwitch>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (currentState != PlayerState.DEAD)
        {
            if (!_gravitySwitch.gravityOn && (currentState == PlayerState.CROUCHING || currentState == PlayerState.CROUCHZOOMING))
            {
                if (currentState == PlayerState.CROUCHING)
                {
                    currentState = PlayerState.STANDING;
                }
                else
                {
                    currentState = PlayerState.ZOOMING;
                }
                _stateChangeRequired = true;
            }

            if (_isReloading)
            {
                if (_currentReloadTime >= _reloadTime)
                {
                    _isReloading = false;
                    _currentReloadTime = 0.0f;
                    _stateChangeRequired = true;
                }
                else
                {
                    _currentReloadTime += Time.deltaTime;
                }
            }

            if ((Input.GetKeyDown(KeyCode.C) || Input.GetButtonDown("B")) && _gravitySwitch.gravityOn)
            {
                if (currentState == PlayerState.CROUCHING)
                {
                    currentState = PlayerState.STANDING;
                }
                else if (currentState == PlayerState.CROUCHZOOMING)
                {
                    currentState = PlayerState.ZOOMING;
                }
                else
                {
                    if (currentState == PlayerState.ZOOMING)
                    {
                        currentState = PlayerState.CROUCHZOOMING;
                    }
                    else
                    {
                        currentState = PlayerState.CROUCHING;
                    }
                }
                _stateChangeRequired = true;
            }

            if (Input.GetMouseButtonDown(1) || (Input.GetButtonDown("RightStickClick") && _rightStickReleased && !_isZoomed))
            {
                _isZoomed = true;
                _rightStickReleased = false;
                if (currentState == PlayerState.CROUCHING)
                {
                    currentState = PlayerState.CROUCHZOOMING;
                }
                else
                {
                    currentState = PlayerState.ZOOMING;
                }
                _stateChangeRequired = true;
            }
            if (Input.GetMouseButtonUp(1) || (Input.GetButtonDown("RightStickClick") && _rightStickReleased && _isZoomed))
            {
                _isZoomed = false;
                _rightStickReleased = false;
                if (currentState == PlayerState.CROUCHZOOMING)
                {
                    currentState = PlayerState.CROUCHING;
                }
                else
                {
                    currentState = PlayerState.STANDING;
                }
                _stateChangeRequired = true;
            }
            if (Input.GetButtonUp("RightStickClick"))
            {
                _rightStickReleased = true;
            }

            if (Input.GetKeyDown(KeyCode.R) &&
                _eventHandler.CurrentWeaponAmmoCount.Get() != _eventHandler.CurrentWeaponMaxAmmoCount.Get())  // Reloading isn't allowed if the player has a full magazine, hence we don't want to transfer into the reloading state if no reloading is occuring.
            {
                PrepareReload();
            }

            if (_stateChangeRequired)
            {
                if (currentState == PlayerState.STANDING)
                {
                    HandleStanding();
                }
                else if (currentState == PlayerState.CROUCHING)
                {
                    HandleCrouching();
                }
                else if (currentState == PlayerState.ZOOMING)
                {
                    HandleZooming();
                }
                else if (currentState == PlayerState.CROUCHZOOMING)
                {
                    HandleCrouchZooming();
                }

                if (_isReloading)
                {
                    HandleReloading();
                }
                _stateChangeRequired = false;
            }

            if (SceneManager.GetActiveScene().name == "GameSceneVR" && UnityEngine.VR.VRSettings.enabled && UnityEngine.VR.VRSettings.loadedDeviceName != "")
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Back"))
                {
                    UnityEngine.VR.InputTracking.Recenter();
                }
            }
        }
    }

    //Handles the changes required when the player is standing up.
    private void HandleStanding()
    {
        _playerController.MotorAcceleration = _MOVE_STANDING;
        _playerInput.MouseLookSensitivity = _SENSITIVITY_NOT_ZOOMING;
        _cameraController.PositionOffset = _CAMERA_POSITION_STANDING;
        _cameraController.RenderingFieldOfView = _CAMERA_FIELD_OF_VIEW_NOT_ZOOMING;

        _playerController.Refresh();
        _playerInput.Refresh();
        _cameraController.Refresh();

        if (pistol.activeSelf)
        {
            _pistolWeapon.PositionOffset = _PISTOL_POSITION_STANDING;
            _pistolShooter.MuzzleFlashPosition = _PISTOL_MUZZLE_FLASH_POSITION_STANDING;
            _pistolShooter.ProjectileSpread = _PISTOL_SPREAD_NOT_ZOOMING;

            _pistolWeapon.Refresh();
            _pistolShooter.Refresh();
        }
        else if (reaper.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _reaperWeapon.PositionOffset = _REAPER_POSITION_VR_STANDING;
            }
            else
            {
                _reaperWeapon.PositionOffset = _REAPER_POSITION_STANDING;
            }
            _reaperShooter.MuzzleFlashPosition = _REAPER_MUZZLE_FLASH_POSITION_STANDING;
            _reaperShooter.ProjectileSpread = _REAPER_SPREAD_NOT_ZOOMING;

            _reaperWeapon.Refresh();
            _reaperShooter.Refresh();
        }
        else if (assaultRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_VR_STANDING;
            }
            else
            {
                _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_STANDING;
            }
            _assaultRifleShooter.MuzzleFlashPosition = _ASSAULT_RIFLE_MUZZLE_FLASH_POSITION_STANDING;
            _reaperShooter.ProjectileSpread = _REAPER_SPREAD_NOT_ZOOMING;

            _assaultRifleWeapon.Refresh();
            _assaultRifleShooter.Refresh();
        }
        else if (laserRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_VR_STANDING;
            }
            else
            {
                _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_STANDING;
            }
            _laserRifleWeapon.Refresh();
        }
    }

    //Handles the changes required when the player is crouching.
    private void HandleCrouching()
    {
        _playerController.MotorAcceleration = _MOVE_CROUCHING;
        _playerInput.MouseLookSensitivity = _SENSITIVITY_NOT_ZOOMING;
        _cameraController.PositionOffset = _CAMERA_POSITION_CROUCHING;
        _cameraController.RenderingFieldOfView = _CAMERA_FIELD_OF_VIEW_NOT_ZOOMING;

        _playerController.Refresh();
        _playerInput.Refresh();
        _cameraController.Refresh();

        if (pistol.activeSelf)
        {
            _pistolWeapon.PositionOffset = _PISTOL_POSITION_CROUCHING;
            _pistolShooter.MuzzleFlashPosition = _PISTOL_MUZZLE_FLASH_POSITION_CROUCHING;
            _pistolShooter.ProjectileSpread = _PISTOL_SPREAD_NOT_ZOOMING;

            _pistolWeapon.Refresh();
            _pistolShooter.Refresh();
        }
        else if (reaper.activeSelf)
        {
            _reaperWeapon.PositionOffset = _REAPER_POSITION_CROUCHING;
            _reaperShooter.MuzzleFlashPosition = _REAPER_MUZZLE_FLASH_POSITION_CROUCHING;
            _reaperShooter.ProjectileSpread = _REAPER_SPREAD_NOT_ZOOMING;

            _reaperWeapon.Refresh();
            _reaperShooter.Refresh();
        }
        else if (assaultRifle.activeSelf)
        {
            _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_CROUCHING;
            _assaultRifleShooter.MuzzleFlashPosition = _ASSAULT_RIFLE_MUZZLE_FLASH_POSITION_CROUCHING;
            _assaultRifleShooter.ProjectileSpread = _ASSAULT_RIFLE_SPREAD_NOT_ZOOMING;

            _assaultRifleWeapon.Refresh();
            _assaultRifleShooter.Refresh();
        }
        else if (laserRifle.activeSelf)
        {
            _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_CROUCHING;
            _laserRifleWeapon.Refresh();
        }
    }

    //Handles the changes required when the player is zooming in.
    private void HandleZooming()
    {
        _playerController.MotorAcceleration = _MOVE_ZOOMING;
        _playerInput.MouseLookSensitivity = _SENSITIVITY_ZOOMING;
        _cameraController.PositionOffset = _CAMERA_POSITION_STANDING;
        _cameraController.RenderingFieldOfView = _CAMERA_FIELD_OF_VIEW_ZOOMING;

        _playerController.Refresh();
        _playerInput.Refresh();
        _cameraController.Refresh();

        if (pistol.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _pistolWeapon.PositionOffset = _PISTOL_POSITION_VR_ZOOMING;
            }
            else
            {
                _pistolWeapon.PositionOffset = _PISTOL_POSITION_ZOOMING;
            }
            _pistolShooter.ProjectileSpread = _PISTOL_SPREAD_ZOOMING;

            _pistolWeapon.Refresh();
            _pistolShooter.Refresh();
        }
        else if (reaper.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _reaperWeapon.PositionOffset = _REAPER_POSITION_VR_ZOOMING;
            }
            else
            {
                _reaperWeapon.PositionOffset = _REAPER_POSITION_ZOOMING;
            }
            _reaperShooter.ProjectileSpread = _REAPER_SPREAD_ZOOMING;

            _reaperWeapon.Refresh();
            _reaperShooter.Refresh();
        }
        else if (assaultRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_VR_ZOOMING;
            }
            else
            {
                _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_ZOOMING;
            }
            _assaultRifleShooter.ProjectileSpread = _ASSAULT_RIFLE_SPREAD_ZOOMING;

            _assaultRifleWeapon.Refresh();
            _assaultRifleShooter.Refresh();
        }
        else if (laserRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_VR_ZOOMING;
            }
            else
            {
                _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_ZOOMING;
            }
            _laserRifleWeapon.Refresh();
        }
    }

    //Handles the changes required when the player is zooming in while crouching.
    private void HandleCrouchZooming()
    {
        _playerController.MotorAcceleration = _MOVE_ZOOMING;
        _playerInput.MouseLookSensitivity = _SENSITIVITY_ZOOMING;
        _cameraController.PositionOffset = _CAMERA_POSITION_CROUCHING;
        _cameraController.RenderingFieldOfView = _CAMERA_FIELD_OF_VIEW_ZOOMING;

        _playerController.Refresh();
        _playerInput.Refresh();
        _cameraController.Refresh();

        if (pistol.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _pistolWeapon.PositionOffset = _PISTOL_POSITION_VR_ZOOMING;
            }
            else
            {
                _pistolWeapon.PositionOffset = _PISTOL_POSITION_ZOOMING;
            }
            _pistolShooter.MuzzleFlashPosition = _PISTOL_MUZZLE_FLASH_POSITION_CROUCHING;
            _pistolShooter.ProjectileSpread = _PISTOL_SPREAD_ZOOMING;

            _pistolWeapon.Refresh();
            _pistolShooter.Refresh();
        }
        else if (reaper.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _pistolWeapon.PositionOffset = _REAPER_POSITION_VR_ZOOMING;
            }
            else
            {
                _pistolWeapon.PositionOffset = _REAPER_POSITION_ZOOMING;
            }
            _reaperShooter.MuzzleFlashPosition = _REAPER_MUZZLE_FLASH_POSITION_CROUCHING;
            _reaperShooter.ProjectileSpread = _REAPER_SPREAD_ZOOMING;

            _reaperWeapon.Refresh();
            _reaperShooter.Refresh();
        }
        else if (assaultRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _pistolWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_VR_ZOOMING;
            }
            else
            {
                _pistolWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_ZOOMING;
            }
            _assaultRifleShooter.MuzzleFlashPosition = _ASSAULT_RIFLE_MUZZLE_FLASH_POSITION_CROUCHING;
            _assaultRifleShooter.ProjectileSpread = _ASSAULT_RIFLE_SPREAD_ZOOMING;

            _assaultRifleWeapon.Refresh();
            _assaultRifleShooter.Refresh();
        }
        else if (laserRifle.activeSelf)
        {
            if (SceneManager.GetActiveScene().name == "GameSceneVR")
            {
                _pistolWeapon.PositionOffset = _LASER_RIFLE_POSITION_VR_ZOOMING;
            }
            else
            {
                _pistolWeapon.PositionOffset = _LASER_RIFLE_POSITION_ZOOMING;
            }
            _laserRifleWeapon.Refresh();
        }
    }

    //Handles the changes requires when the player is reloading their weapon.
    private void HandleReloading()
    {
        if (pistol.activeSelf)
        {
            _pistolWeapon.PositionOffset = _PISTOL_POSITION_RELOADING;
            _pistolWeapon.Refresh();
        }
        else if (reaper.activeSelf)
        {
            _reaperWeapon.PositionOffset = _REAPER_POSITION_RELOADING;
            _reaperWeapon.Refresh();
        }
        else if (assaultRifle.activeSelf)
        {
            _assaultRifleWeapon.PositionOffset = _ASSAULT_RIFLE_POSITION_RELOADING;
            _assaultRifleWeapon.Refresh();
        }
        else if (laserRifle.activeSelf)
        {
            _laserRifleWeapon.PositionOffset = _LASER_RIFLE_POSITION_RELOADING;
            _laserRifleWeapon.Refresh();
        }
    }

    private void PrepareReload()
    {
        _isReloading = true;
        if (pistol.activeSelf)
        {
            _reloadTime = pistol.GetComponent<vp_WeaponReloader>().ReloadDuration;
        }
        else if (reaper.activeSelf)
        {
            _reloadTime = reaper.GetComponent<vp_WeaponReloader>().ReloadDuration;
        }
        else if (assaultRifle.activeSelf)
        {
            _reloadTime = assaultRifle.GetComponent<vp_WeaponReloader>().ReloadDuration;
        }
        else if (laserRifle.activeSelf)
        {
            _reloadTime = laserRifle.GetComponent<vp_WeaponReloader>().ReloadDuration;
        }
        _stateChangeRequired = true;
    }

    public void HandleDeath()
    {
        currentState = PlayerState.DEAD;

        _playerController.MotorAcceleration = _MOVE_DEAD;
        _cameraController.PositionOffset = _CAMERA_POSITION_DEAD;

        _playerController.Refresh();
        _cameraController.Refresh();

        if (pistol.activeSelf)
        {
            pistol.SetActive(false);
        }
        else if (reaper.activeSelf)
        {
            reaper.SetActive(false);
        }
        else if (assaultRifle.activeSelf)
        {
            assaultRifle.SetActive(false);
        }
        else if (laserRifle.activeSelf)
        {
            laserRifle.SetActive(false);
        }
    }
}
