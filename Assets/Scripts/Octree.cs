﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Octree : MonoBehaviour
{
	//The maximum number of objects that can be held in a single node of the octree before it needs to split.
	//public static int MAX_OBJECT_LIMIT = 3;

	//The maximum size that a single node can be.
	public static Vector3 MAX_NODE_SIZE = new Vector3(2f, 2f, 2f); 

	//Getter for the root of the octree.
	public static OctreeNode root
	{
		get
		{
			if (_root == null)
			{
				_arenaTrigger = GameObject.FindGameObjectWithTag("ArenaTrigger").GetComponent<BoxCollider>();
				_root = new OctreeNode(null, new Vector3(-55f, 50f, 95f), 32f, null);
				//_root.DrawNodeGameObject(Color.red);
				++totalNodes;
				_root.Breakdown(ref _arenaTrigger);
				//Debug.Log("Total Nodes: " + totalNodes);
				mortonCodes.Sort();
				_root.SetNeighbourNodes();
			}
			return _root;
		}
	}

	//A list of all the morton codes associated with each leaf node. The list is used to find the neighbours of a given node.
	public static List<MortonCode> mortonCodes = new List<MortonCode>();

	//The number of children that were combined back together during the octree collapse process.
	public static int nodesCombined = 0;

	//The total number of nodes currently existing in the tree.
	public static int totalNodes = 0;


	//The root node of the octree. The root of the tree will always be the same for each node, so we can have one instance shared between all nodes.
	private static OctreeNode _root;

	//The trigger volume that surrounds the main arena. Used to determine if a node lives within the arena, or if it needs to be culled on that basis.
	private static BoxCollider _arenaTrigger;


	[RuntimeInitializeOnLoadMethod]
	static bool Init()
	{
		//Will call the getter for the public root variable, which will create it at the same time, and by extension return whether the creation was successful.
		return root == null;
	}

	void Start()
	{
		//StartCoroutine(LateStart());
	}

    //Used for various tests performed while implementing the octree.
	private IEnumerator LateStart()
	{
		//OctreeNode start = mortonCodes[mortonCodes.Count / 2].associatedNode;
		//OctreeNode end = mortonCodes[1523].associatedNode;

		//List<OctreeNode> path = AStar.AStarPathfinding(start, end);
		//Debug.Log(path.Count);

		//start.DrawNodeGameObject(Color.blue);
		//end.DrawNodeGameObject(Color.red);
		//foreach (OctreeNode pathNode in path)
		//{
		//	pathNode.DrawNodeGameObject(Color.green);
		//}

		//mortonCodes[1234].associatedNode.DrawNodeGameObject(Color.blue);
		//foreach (OctreeNode node in mortonCodes[1234].associatedNode.neighbours)
		//{
		//	node.DrawNodeGameObject(Color.red);
		//}

		//yield return new WaitForSeconds(0.5f); //Give time for all items to insert themselves into the octree.
		//_root.CombineEmptyNodes();
		//nodes.Sort();

		yield return new WaitForEndOfFrame();
	}
}
