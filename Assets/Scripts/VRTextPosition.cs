﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Contains values for where various text should be positioned in the world when playing the game in VR.
public class VRTextPosition : MonoBehaviour
{
    public Camera m_camera;

    public Text waveCountText;
    public Text gravitySwitchText;
    public Text enemiesRemainingText;

    public Text healthText;
    public Text healthPackText;
    public Text batteryText;
    public Text ammoText;

    public GameObject healthTexture;
    public GameObject healthPackTexture;
    public GameObject batteryTexture;

    public Text buyItemText;
    public Text itemCostText;
    public Text overText;
    public Text newWaveWarning;
	
	// Update is called once per frame
	void Update ()
    {
        waveCountText.transform.position = m_camera.transform.position;
        waveCountText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        waveCountText.transform.position += m_camera.transform.forward * 400f;
        waveCountText.transform.position += m_camera.transform.up * 175f;

        gravitySwitchText.transform.position = m_camera.transform.position;
        gravitySwitchText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        gravitySwitchText.transform.position += m_camera.transform.forward * 400f;
        gravitySwitchText.transform.position += m_camera.transform.up * 155f;

        enemiesRemainingText.transform.position = m_camera.transform.position;
        enemiesRemainingText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        enemiesRemainingText.transform.position += m_camera.transform.forward * 400f;
        enemiesRemainingText.transform.position += m_camera.transform.up * 135f;

        healthTexture.transform.position = m_camera.transform.position;
        healthTexture.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        healthTexture.transform.position += m_camera.transform.forward * 400f;
        healthTexture.transform.position += m_camera.transform.up * 55f;
        healthTexture.transform.position -= m_camera.transform.right * 150f;

        healthText.transform.position = m_camera.transform.position;
        healthText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        healthText.transform.position += m_camera.transform.forward * 400f;
        healthText.transform.position += m_camera.transform.up * 55f;
        healthText.transform.position -= m_camera.transform.right * 130f;

        healthPackTexture.transform.position = m_camera.transform.position;
        healthPackTexture.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        healthPackTexture.transform.position += m_camera.transform.forward * 400f;
        healthPackTexture.transform.position += m_camera.transform.up * 55f;
        healthPackTexture.transform.position -= m_camera.transform.right * 80f;

        healthPackText.transform.position = m_camera.transform.position;
        healthPackText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        healthPackText.transform.position += m_camera.transform.forward * 400f;
        healthPackText.transform.position += m_camera.transform.up * 55f;
        healthPackText.transform.position -= m_camera.transform.right * 60f;

        batteryTexture.transform.position = m_camera.transform.position;
        batteryTexture.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        batteryTexture.transform.position += m_camera.transform.forward * 400f;
        batteryTexture.transform.position += m_camera.transform.up * 85f;
        batteryTexture.transform.position -= m_camera.transform.right * 150f;

        batteryText.transform.position = m_camera.transform.position;
        batteryText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        batteryText.transform.position += m_camera.transform.forward * 400f;
        batteryText.transform.position += m_camera.transform.up * 85f;
        batteryText.transform.position -= m_camera.transform.right * 130f;

        ammoText.transform.position = m_camera.transform.position;
        ammoText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        ammoText.transform.position += m_camera.transform.forward * 400f;
        ammoText.transform.position += m_camera.transform.up * 85f;
        ammoText.transform.position -= m_camera.transform.right * 25f;

        buyItemText.transform.position = m_camera.transform.position;
        buyItemText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        buyItemText.transform.position += m_camera.transform.forward * 400f;

        itemCostText.transform.position = m_camera.transform.position;
        itemCostText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        itemCostText.transform.position += m_camera.transform.forward * 400f;
        itemCostText.transform.position -= m_camera.transform.up * 30f;

        overText.transform.position = m_camera.transform.position;
        overText.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        overText.transform.position += m_camera.transform.forward * 400f;
        overText.transform.position += m_camera.transform.up * 50f;

        newWaveWarning.transform.position = m_camera.transform.position;
        newWaveWarning.transform.rotation = Quaternion.LookRotation(m_camera.transform.forward);
        newWaveWarning.transform.position += m_camera.transform.forward * 400f;
        newWaveWarning.transform.position += m_camera.transform.up * 50f;
    }
}
