﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageFlash : MonoBehaviour
{
    //Player event handler used to send a message to the HUD to flash as a result of damage inflicted.
	private vp_FPPlayerEventHandler _playerEventHandler;


    // Use this for initialization
    void Start ()
    {
        _playerEventHandler = GameObject.FindGameObjectWithTag("Player").GetComponent<vp_FPPlayerEventHandler>();
    }

	//Sends a signal to the HUD to flash. Also sends the location of the enemy so an arrow can point the player in the direction of the shot.
    public void FlashWhenHit(Transform enemyLocation)
    {
        _playerEventHandler.HUDDamageFlash.Send(new vp_DamageInfo(1, enemyLocation));
    }
}
