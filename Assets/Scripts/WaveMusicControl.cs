﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class WaveMusicControl : MonoBehaviour
{
    //The audio snapshot that has the combat music on and the in-between wave music off.
    public AudioMixerSnapshot waveOn;

    //The audio snapshot that has the combat music off and the in-between wave music on.
    public AudioMixerSnapshot waveOff;

    //The audio snapshot that lowers the wave off music volume and plays a failure sting if the player is in the restock area too long.
    public AudioMixerSnapshot outOfArena;

    //The audio snapshot that lowers the wave on music volume and plays a failure sting if the player dies.
    public AudioMixerSnapshot death;

    //Short audio clips to be played on game over (either by death, or be being in the restock area too long).
    public AudioClip[] gameOverStings;

    //The audio source for the wave on music.
    public AudioSource waveOnMusic;

    //The audio source for the wave off music.
    public AudioSource waveOffMusic;

    //The audio source for the game over sting sounds.
    public AudioSource stingSource;

    //The beats per minute of the music. Used to calculate the transition times;
    public float bpm = 90;


    //The time to spend transitioning into the wave on music.
	private float _transitionCombat;

    //The time to spend transitioning into the in-between wave music.
	private float _transitionBreak;

    //The length of a quarter note, based on the beats per minute. Used to calculate the transition times.
	private float _quarterNote;


	// Use this for initialization
	void Start ()
    {
        _quarterNote = 60 / bpm;
        _transitionCombat = _quarterNote;
        _transitionBreak = _quarterNote * 4;
	}

    //Transitions out of the wave off music into the wave on music.
    public void TransitionIntoWave()
    {
        waveOnMusic.Play();
        waveOn.TransitionTo(_transitionCombat);
    }

    //Transitions out of the wave on music into the wave off music.
    public void TransitionOutOfWave()
    {
        waveOffMusic.Play();
        waveOff.TransitionTo(_transitionBreak);
    }

    //Transitions to the out of arena failure state music.
    public void TransitionToOutOfArena()
    {
        outOfArena.TransitionTo(0); //We want to transition immediately.
        PlayGameOverSting();
    }

    //Transitions to the death music.
    public void TransitionToDeath()
    {
        death.TransitionTo(0); //We want to transition immediately.
        PlayGameOverSting();
    }

    //Randomly chooses one of the game over stings and plays it.
    public void PlayGameOverSting()
    {
        int randClip = Random.Range(0, gameOverStings.Length);
        stingSource.clip = gameOverStings[randClip];
        stingSource.Play();
    }
}
