﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadGame : MonoBehaviour
{
    /* Not used anymore. Essentially useless because the bulk of the loading time is generating the octree, so the bar would immediately get there, freeze,
     * then immediately finish once that was done. No useful feedback was provided to the player.
    //The empty progress bar.
    public Texture2D emptyProgressBar;

    //The full progress bar. Will be partially laid over the empty bar based on how much progress
    //has been made in loading the game.
    public Texture2D fullProgressBar;
    */

    //The asynchronous loading operation
	private AsyncOperation _async = null;

    
    //Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine(LoadScene());
    }

    private IEnumerator LoadScene()
    {
        _async = SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
        yield return _async;
    }

    /*
    void OnGUI()
    {
        if (_async != null)
        {
            GUI.DrawTexture(new Rect(Screen.width / 2 - Screen.width / 5, Screen.height / 10 * 6, emptyProgressBar.width, emptyProgressBar.height), emptyProgressBar);
            GUI.DrawTexture(new Rect(Screen.width / 2 - Screen.width / 5, Screen.height / 10 * 6, fullProgressBar.width * _async.progress, fullProgressBar.height), fullProgressBar);
        }
    }
    */
}
