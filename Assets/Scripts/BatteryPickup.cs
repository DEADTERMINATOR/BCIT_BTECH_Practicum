﻿using UnityEngine;
using System.Collections;

public class BatteryPickup : MonoBehaviour 
{
    //The amount of battery power the pickup gives the player.
	public int amountOfBatteryPower;


    //Reference to the battery display to update it.
	private GUIHealthAndBatteryDisplay playerBattery;


	// Use this for initialization
	void Start () 
	{
		playerBattery = GameObject.FindGameObjectWithTag("HealthAndBatteryInfo").GetComponent<GUIHealthAndBatteryDisplay>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(new Vector3(-0.5f, -0.5f, -0.5f)); 
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			playerBattery.totalBatteryAmount += amountOfBatteryPower;
			Destroy(gameObject);
		}
	}
}
