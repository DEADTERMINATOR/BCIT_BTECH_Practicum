﻿using UnityEngine;
using System.Collections;

public class PlayerDamageHandler : MonoBehaviour
{
	//How much damage a single bullet from the enemy does.
	private const int _BULLET_DAMAGE = 1;


	//Reference to the health and battery display so we can update the player's health if the shot hits and damage needs to be dealt.
	private GUIHealthAndBatteryDisplay _healthDisplay;

	//Reference to the player state manager used to get the player's current state to see if their stance affects the base hit chance.
	private PlayerStateManager _playerStateManager;

	//Reference to the damage flash script used to send a flash if the shot hits.
	private DamageFlash _damageFlash;


	// Use this for initialization
	void Start ()
    {
        _healthDisplay = GameObject.FindGameObjectWithTag("HealthAndBatteryInfo").GetComponent<GUIHealthAndBatteryDisplay>();
        _playerStateManager = GetComponent<PlayerStateManager>();
        _damageFlash = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<DamageFlash>();
	}

    public bool AttemptDamage(float distance, bool isMoving, Transform enemyLocation)
    {
        float baseChance = 100 - distance * 3;
        if (isMoving)
        {
            baseChance *= EnemyBehaviour.PLAYER_MOVING_ACCURACY_MODIFIER;
        }
        else if (_playerStateManager.currentState == PlayerStateManager.PlayerState.CROUCHING || _playerStateManager.currentState == PlayerStateManager.PlayerState.CROUCHZOOMING)
        {
            baseChance *= EnemyBehaviour.PLAYER_CROUCHING_ACCURACY_MODIFIER;
        }
        //else if (isSlow)
        //{
        //    baseChance *= EnemyBehaviour.PLAYER_SLOW_MOVING_ACCURACY_MODIFIER;
        //}

        if (baseChance < 0)
        {
            baseChance = 0;
        }

        bool hitSuccessful = false;
        float hitChance = Random.Range(0, 100);
        if (hitChance <= baseChance)
        {
            hitSuccessful = true;
			_healthDisplay.health -= _BULLET_DAMAGE;
			if (_damageFlash != null)
			{
				_damageFlash.FlashWhenHit(enemyLocation);
			}
        }
        return hitSuccessful;

    }
}
