﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseMenuControl : MonoBehaviour
{
    //The pause menu panel. Used to trigger whether the menu is currently visible.
    public GameObject pauseMenuPanel;


    //Is the game currently paused and the menu being displayed.
	private bool _isCurrentlyPaused = false;

	//Reference to the player event handler so we can send a pause signal.
	private vp_FPPlayerEventHandler _playerEventHandler;

	//Reference to camera so we can start and stop camera movement depending on the pause state.
	private vp_FPCamera _camera;

	//Reference to the player's input so we release or lock the cursor depending on the pause state.
	private vp_FPInput _input;


	// Use this for initialization
	void Start ()
    {
        pauseMenuPanel.SetActive(false);
        _playerEventHandler = GameObject.FindGameObjectWithTag("Player").GetComponent<vp_FPPlayerEventHandler>();
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<vp_FPCamera>();
        _input = GameObject.FindGameObjectWithTag("Player").GetComponent<vp_FPInput>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.P) || Input.GetButtonDown("Start"))
        {
            if (!_isCurrentlyPaused)
            {
                _playerEventHandler.Pause.Set(true);
                vp_Gameplay.IsPaused = true;
                if (_camera != null)
                {
                    _camera.SetState("Freeze", true);
                }

                pauseMenuPanel.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                _input.MouseCursorForced = true;
				if (!ReferenceEquals(GameObject.FindGameObjectWithTag("Player").GetComponent<EnemyDestructionManager>(), null))
				{
					EnemyDestructionManager.pauseUpdating = true;
				}
                _isCurrentlyPaused = true;
            }
            else
            {
                ContinueGame();
            }
        }
	}

    //Public method used so the player can continue the game via a button press. The player can also continue the game by pressing the pause button again.
    public void ContinueGame()
    {
        _playerEventHandler.Pause.Set(false);
        vp_Gameplay.IsPaused = false;
        if (_camera != null)
        {
            _camera.SetState("Freeze", false);
        }

        pauseMenuPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        _input.MouseCursorForced = false;
		if (!ReferenceEquals(GameObject.FindGameObjectWithTag("Player").GetComponent<EnemyDestructionManager>(), null))
		{
			EnemyDestructionManager.pauseUpdating = false;
		}
        _isCurrentlyPaused = false;
    }

    public void QuitGame()
    {
        ContinueGame();
        LoadScene.LoadByIndex(0);
    }
}
