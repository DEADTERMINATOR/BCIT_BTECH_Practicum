﻿using UnityEngine;
using System.Collections;

public class EnemyMuzzleFlash : MonoBehaviour
{
    public GameObject muzzleFlash;


    private GameObject instantiatedMuzzleFlash;

    public void Activate()
    {
        if (instantiatedMuzzleFlash != null)
        {
            Deactivate();
        }
        instantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, transform.position, transform.rotation);
    }

    public void Deactivate()
    {
        if (instantiatedMuzzleFlash != null)
        {
            Destroy(instantiatedMuzzleFlash);
        }
    }
}
