﻿using UnityEngine;
using System.Collections;

//Quits the application, or exits back to the editor if running in editor mode.
public class QuitOnClick : MonoBehaviour
{
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
