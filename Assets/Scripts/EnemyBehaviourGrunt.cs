﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class EnemyBehaviourGrunt : EnemyBehaviour
{
    //ID counter that increments with each instantiation of a grunt enemy. 
    public static int ID = 0;


    //The minimum and maximum amount of time the enemy will wait at cover before shooting.
	private const float _MINIMUM_WAIT_TIME = 0.5f;
	private const float _MAXIMUM_WAIT_TIME = 1.0f;

	//The base chance of an enemy not at a cover point moving after a shooting bout.
	private const int _BASE_CHANCE_OF_MOVING = 10;

    //The amount the chance of an enemy not at a cover point moving after a shooting bout increases everytime they do not move.
	private const int _INCREASE_CHANCE_OF_MOVING = 25;

    //The minimum and maximum amount of time the enemy will wait before moving if they are unable to hit the player.
    //We don't want the enemy to move the moment they lose sight of the player (e.g. the player ducks behind cover), as that is
    //not realistic.
	private const float _MINIMUM_TIME_BEFORE_MOVE = 7.5f;
	private const float _MAXIMUM_TIME_BEFORE_MOVE = 10.0f;

    //The number of bullets fired in a single bout.
	private const int _NUM_TIMES_SHOOT_PER_BOUT = 1;

	//Offset for the muzzle flash effect when the enemy fires their weapon.
	private Vector3 _MUZZLE_FLASH_OFFSET = new Vector3(0f, 2.25f, 0f);

	private float _LOCATION_ARRIVAL_RADIUS = 1.5f;


    //Enum of possible states the enemy could be in.
	public enum State { SPAWNED_GROUND, SPAWNED_AIR, MOVING_GROUND, MOVING_AIR, SHOOTING, AT_WAYPOINT, AT_COVER_WAYPOINT, NEEDS_NEW_WAYPOINT_GROUND, NEEDS_NEW_WAYPOINT_AIR, DEAD, FIND_NEAREST_WAYPOINT };


	//Which state the enemy is currently in.
	public State currentState;

	//The version of the enemy model that had ragdoll functionality. On death, we switch out the enemy that died with a
	//version that has ragdoll functionality. We do this because of an issue with the ragdoll version of the enemy model in
	//which the body part colliders don't properly follow the model, but the non-ragdoll version is fine. So we use the non-ragdoll
	//version for when the enemy is alive so bullets will collide properly with the enemy, and the ragdoll version for when they are dead
	//so we can get accurate death functionality.
	public GameObject ragdollModel;

	//Tracks whether the gravity shift has been handled, so we only have to do certain things such as changing ragdoll gravity once.
	public bool gravitySwitchNeeded = false;


    //The previous state the enemy was in. Used to know what state to return to when the enemy transfers to the SHOOTING state.
	private State _previousState;

    //A randomly determined amount of time between MINIMUM_WAIT_TIME and MAXIMUM_WAIT_TIME that the enemy will either 
    //sit behind cover for before shooting.
	private float _waitTime;

    //References to the rigidbodies on each of the joints of the enemy.
	private Rigidbody[] _ragdollJoints;

    //The position the bullets will be spawned from.
	private Transform _shootingPosition;

	//Holds whether the non-ragdoll equipped enemy been swapped for a ragdoll equipped one. We only want to do this once, otherwise we will spawn
	//a new enemy everytime the dead body is shot.
	private bool _ragdollSwitchOccurred = false;

    //The instantiated version of the enemy's muzzle flash.
	private GameObject _instantiatedMuzzleFlash;

    //Has the enemy waited the appropriate amount of time after gravity is reactivated (see WaitForGravity()).
	private bool _waitedForGravity = true;

    //Holds the current path from one air waypoint to another.
	private List<OctreeNode> _airPath;

    //The previous waypoint occupied by the enemy.
	private Waypoint _previousWaypoint;

    //Universal ID identifying a particular instance of a grunt enemy. 
    private int _id;

    //The sounds the enemy may play upon death.
    private AudioSource enemyDeathGrunt1;
    private AudioSource enemyDeathGrunt2;

    //Debug variables used to test the average hit percentage.
    private int _debugShotsFired = 0;
    private int _debugShotsHit = 0;


    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _id = ID++;
        _ragdollJoints = rigidBody.GetComponentsInChildren<Rigidbody>();
        _waitTime = Random.Range(_MINIMUM_WAIT_TIME, _MAXIMUM_WAIT_TIME);
        timeBeforeMove = Random.Range(_MINIMUM_TIME_BEFORE_MOVE, _MAXIMUM_TIME_BEFORE_MOVE);
        _shootingPosition = GetComponentInChildren<EnemyMuzzleFlash>().gameObject.transform;
        //_shootingPosition = GameObject.FindGameObjectWithTag("GruntShootingPosition").transform;
        enemyDeathGrunt1 = GetComponents<AudioSource>()[1];
        enemyDeathGrunt2 = GetComponents<AudioSource>()[2];
		SetKinematicState(true);

		if (!gravitySwitch.gravityOn)
		{
			currentState = State.SPAWNED_AIR;
		}
		else
		{
			currentState = State.SPAWNED_GROUND;
		}
		gravitySwitchNeeded = true;

        //StartCoroutine(FindPathToAirWaypoint_TestVersion(GameObject.Find("AirSpawnWaypoint2").GetComponent<Waypoint>(), GameObject.Find("AirWaypoint (25)").GetComponent<Waypoint>()));
    }
	
	// Update is called once per frame
	void Update()
    {
		if (!isDead)
		{
            /*
            if (_id == 0)
            {
                float distance = CalculateEuclideanDistance(transform, target);
                if (distance >= 25.0f && distance <= 26.0f)
                {
                    Debug.Log("Distance From Player: " + distance);
                    Debug.Log("Shots Fired: " + _debugShotsFired);
                    Debug.Log("Shots Hit: " + _debugShotsHit);
                }
                else
                {
                    Debug.Log("Not in Range");
                }
            }
            */

			if (gravitySwitch.gravityOn)
			{
				if (gravitySwitchNeeded)
				{
					SetRagdollGravity(true);
					SetKinematicState(false);
					_waitedForGravity = false;
					StartCoroutine(WaitForGravity());
					gravitySwitchNeeded = false;
					_airPath = null;

                    //If the enemy was travelling to their initial spawn point when the switch occured, we need
                    //to switch their new spawn point to the ground variant. This also ends up being run as part of the initial
                    //ragdoll and kinematic state setup on spawn, hence the check for and resetting of SPAWNED_GROUND variables.
                    if (currentState == State.SPAWNED_AIR || currentState == State.SPAWNED_GROUND)
					{
                        currentState = State.SPAWNED_GROUND;
                        currentWaypoint.isOccupied = false; //The current waypoint is still pointing to their air spawn waypoint, so we mark that as unoccupied.
                        EnemySpawn enemySpawn = GameObject.FindGameObjectWithTag("Player").GetComponent<EnemySpawn>();
                        if (enemySpawn.spawnSide == 1)
                        {
                            currentWaypoint = enemySpawn.groundSpawnPoint1StartingWaypoint;
                        }
                        else
                        {
                            currentWaypoint = enemySpawn.groundSpawnPoint2StartingWaypoint;
                        }
                        currentWaypoint.isOccupied = true;
                    }
					else
					{
						currentState = State.FIND_NEAREST_WAYPOINT;
					}
				}

				if (_waitedForGravity)
				{
					if (currentState == State.SPAWNED_GROUND)
					{
						anim.SetFloat("Speed", moveSpeed);
						anim.SetBool("OnGround", true);
						anim.SetBool("Aiming", false);

						if (!navMeshAgent.pathPending)
						{
							if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
							{
                                navMeshAgent.acceleration = 60.0f;
								if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
								{
									currentState = State.NEEDS_NEW_WAYPOINT_GROUND;
								}
							}  
						}
						else if (navMeshAgent.pathStatus == NavMeshPathStatus.PathInvalid || navMeshAgent.pathStatus == NavMeshPathStatus.PathPartial)
						{
							SetNewNavPosition();
						}
					}
					else if (currentState == State.MOVING_GROUND)
					{
						anim.SetFloat("Speed", moveSpeed);
						anim.SetBool("OnGround", true);
						anim.SetBool("Aiming", false);

						if (!navMeshAgent.pathPending)
						{
							if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
							{
								if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
								{
									if (currentWaypoint.isCover)
									{
										currentState = State.AT_COVER_WAYPOINT;
									}
									else
									{
										currentState = State.AT_WAYPOINT;
									}
								}
							}  
						}
						else if (navMeshAgent.pathStatus == NavMeshPathStatus.PathInvalid || navMeshAgent.pathStatus == NavMeshPathStatus.PathPartial)
						{
							SetNewNavPosition();
						}
					}
					else if (currentState == State.AT_COVER_WAYPOINT)
					{
						anim.SetFloat("Speed", 0.0f);
						anim.SetBool("Aiming", true);
						anim.SetTrigger("Shoot");

						if (_waitTime <= 0.0f)
						{
							currentState = State.SHOOTING;
							_previousState = State.AT_COVER_WAYPOINT;
							_waitTime = Random.Range(_MINIMUM_WAIT_TIME, _MAXIMUM_WAIT_TIME);
						}
						else
						{
							transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), Time.deltaTime);
							_waitTime -= Time.deltaTime;
						}
					}
					else if (currentState == State.NEEDS_NEW_WAYPOINT_GROUND)
					{
						List<Waypoint> neighbours = currentWaypoint.neighbours; //A local copy of the current waypoint's neighbours.
						List<Waypoint> possibleCoverPoints = new List<Waypoint>(); //Add any valid cover neighbours so we can randomly choose.
						List<Waypoint> possiblePoints = new List<Waypoint>(); //Add any valid neighbours so we can randomly choose. Only used if there are no valid cover points.

						foreach (Waypoint waypoint in neighbours)
						{
							if (!waypoint.isOccupied)
							{
								if (waypoint.isCover)
								{
									//If the waypoint is a cover point, we need to test if the player can see it and if the enemy can see the player. 
									//If the player can see the point, it's useless cover. If the enemy can't see the player, they essentially stop participating
									//in the battle, which is less interesting for the player.
									RaycastInfo playerSeesCoverInfo = CanPlayerSeeCoverPoint(waypoint);
									RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(waypoint, true);
									if (!playerSeesCoverInfo.canSeeTarget && enemySeesPlayerInfo.canSeeTarget)
									{
										possibleCoverPoints.Add(waypoint);
									}
								}
								else
								{
									//If the waypoint is not a cover point, we only care if the enemy can see the player from the waypoint.
									RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(waypoint, false);
									if (enemySeesPlayerInfo.canSeeTarget)
									{
										possiblePoints.Add(waypoint);
									}
								}
							}
						}

						Waypoint newWaypoint = null;
						if (possibleCoverPoints.Count != 0 || possiblePoints.Count != 0)
						{
							if (possibleCoverPoints.Count > 1)
							{
								int randomCoverPoint = Random.Range(0, possibleCoverPoints.Count);
								newWaypoint = possibleCoverPoints[randomCoverPoint];
							}
							else if (possibleCoverPoints.Count == 1)
							{
								newWaypoint = possibleCoverPoints[0];
							}
							else
							{
								int randomPoint = Random.Range(0, possiblePoints.Count);
								newWaypoint = possiblePoints[randomPoint];
							}
						}
						else
						{
							Collider[] waypointsNearPlayer = FindGroundWaypointsNearPosition(target.position);

							int randomPoint;
							if (waypointsNearPlayer.Length != 0)
							{
								//We have at least one waypoint near the player.
								if (waypointsNearPlayer.Length > 1)
								{
									randomPoint = Random.Range(0, waypointsNearPlayer.Length);
									newWaypoint = waypointsNearPlayer[randomPoint].gameObject.GetComponent<Waypoint>();
								}
								else
								{
									newWaypoint = waypointsNearPlayer[0].gameObject.GetComponent<Waypoint>();
								}
							}
							else if (waypointsNearPlayer.Length == 0 && neighbours.Count != 0)
							{
								//Unbelievably, there are no waypoints near the player. So let's go to one of the current waypoint's neighbours and try everything again.
								if (neighbours.Count > 1)
								{
									randomPoint = Random.Range(0, neighbours.Count);
									newWaypoint = neighbours[randomPoint];
								}
								else
								{
									newWaypoint = neighbours[0];
								}
							}
							else
							{
								//Some crazy BS is going on and apparently there no waypoints near the player and the current waypoint has no neighbours. Just keep shooting like a moron.
								currentState = State.SHOOTING;
							}
						}

						if (newWaypoint != null)
						{
							_previousWaypoint = currentWaypoint;
							_previousWaypoint.isOccupied = false;
							currentWaypoint = newWaypoint;
							newWaypoint.isOccupied = true;
							currentState = State.MOVING_GROUND;
							SetNewNavPosition();
							arrivedAtWaypoint = false;
						}
					}
				}
			}
			else if (!gravitySwitch.gravityOn)
			{
				if (gravitySwitchNeeded)
				{
					SetRagdollGravity(false);
					SetKinematicState(true);
					if (navMeshAgent != null)
					{
                        //Clear any partially complete path and stop the nav mesh agent functionality,
                        //as it prevents the enemy from floating in zero-g if it remains active.
                        if (navMeshAgent.isActiveAndEnabled)
                        {
                            navMeshAgent.ResetPath();
                            navMeshAgent.Stop();
                        }
						navMeshAgent.enabled = false;
					}
					gravitySwitchNeeded = false;

					if (currentState == State.SPAWNED_GROUND || currentState == State.SPAWNED_AIR)
					{
                        //If the enemy was travelling to their initial spawn point when the switch occured, we need
                        //to switch their new spawn point to the air variant. This also ends up being run as part of the initial
                        //ragdoll and kinematic state setup, hence the check for and resetting of SPAWNED_AIR variables.
                        currentState = State.SPAWNED_AIR;
                        currentWaypoint.isOccupied = false; //The current waypoint is still pointing to their ground spawn waypoint, so we mark that as unoccupied.
                        EnemySpawn enemySpawn = GameObject.FindGameObjectWithTag("Player").GetComponent<EnemySpawn>();
                        if (enemySpawn.spawnSide == 1)
                        {
                            currentWaypoint = enemySpawn.airSpawnPoint1StartingWaypoint;
                        }
                        else
                        {
                            currentWaypoint = enemySpawn.airSpawnPoint2StartingWaypoint;
                        }
                        currentWaypoint.isOccupied = true; //The current waypoint now points to their air spawn waypoint, so we mark that as occupied.
                    }
					else
					{
						currentState = State.FIND_NEAREST_WAYPOINT;
					}
				}

				if (currentState == State.SPAWNED_AIR)
				{
					anim.SetFloat("Speed", 0f);
					anim.SetBool("OnGround", false);
					Vector3 targetWaypointPosition = new Vector3(currentWaypoint.transform.position.x, transform.position.y, currentWaypoint.transform.position.z);
					transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetWaypointPosition - transform.position), Time.deltaTime);

					MoveTowardsAirWaypoint(currentWaypoint.transform.position);
					//if (GameObject.FindGameObjectWithTag("ArenaTrigger").GetComponent<BoxCollider>().bounds.Contains(transform.position))
                    if (EnemyNearCurrentAirWaypoint())
					{
                        arrivedAtWaypoint = true;
                        currentState = State.NEEDS_NEW_WAYPOINT_AIR;
					}
				}
				else if (currentState == State.MOVING_AIR)
				{
					anim.SetFloat("Speed", 0f);
					anim.SetBool("OnGround", false);
					anim.SetBool("Aiming", false);

					if (_airPath != null && _airPath.Count != 0)
					{
						//foreach (OctreeNode node in _airPath)
						//{
						//	node.DrawNodeGameObject(Color.green);
						//}
						OctreeNode targetNode = _airPath[_airPath.Count - 1];
						transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetNode.position - transform.position), Time.deltaTime);
						MoveTowardsAirWaypoint(targetNode.position);
						if (EnemyNearOctreePoint(targetNode.position))
						{
							_airPath.Remove(targetNode);
							if (EnemyNearCurrentAirWaypoint())
							{
								arrivedAtWaypoint = true;
								currentState = State.AT_WAYPOINT;
							}
						}
					}
                    else
                    {
                        currentState = State.FIND_NEAREST_WAYPOINT;
                    }
				}
				else if (currentState == State.NEEDS_NEW_WAYPOINT_AIR)
				{
					List<Waypoint> neighbours = currentWaypoint.neighbours;

					float shortestDistance = float.MaxValue;
					Waypoint closestWaypoint = null;
					foreach (Waypoint airWaypoint in neighbours)
					{
						RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(airWaypoint, false);
						if (!enemySeesPlayerInfo.canSeeTarget || ReferenceEquals(airWaypoint, currentWaypoint) || airWaypoint.isOccupied)
						{
							continue;
						}
						float distanceFromPlayer = CalculateEuclideanDistance(airWaypoint.transform, target);
						if (distanceFromPlayer < shortestDistance)
						{
							shortestDistance = distanceFromPlayer;
							closestWaypoint = airWaypoint;
						}
					}

					if (closestWaypoint == null)
					{
						closestWaypoint = neighbours[0];
					}

					_previousWaypoint = currentWaypoint;
					currentWaypoint = closestWaypoint;
					closestWaypoint.isOccupied = true;
					currentState = State.MOVING_AIR;
					arrivedAtWaypoint = false;
					StartCoroutine(FindPathToAirWaypoint());
				}
			}
			if (currentState == State.SHOOTING)
			{
				anim.SetBool("Aiming", true);
				anim.SetTrigger("Shoot");

				transform.LookAt(target);
				if (shootTime <= 0.0f && readyToFire)
				{
				    currentState = _previousState;
					shootTime = Random.Range(MINIMUM_SHOOT_TIME, MAXIMUM_SHOOT_TIME);
                    stayedAtWaypoint = true;
                }
				else
				{
					if (readyToFire)
					{
						readyToFire = false;
						StartCoroutine(WaitBetweenShots());
					}
					shootTime -= Time.deltaTime;
				}

				RaycastInfo enemySeesPlayerInfo = CanEnemySeePlayer(currentWaypoint, currentWaypoint.isCover);
				if (!enemySeesPlayerInfo.canSeeTarget)
				{
					currentTimeBeforeMove += Time.deltaTime;
					if (currentTimeBeforeMove >= timeBeforeMove)
					{
						if (gravitySwitch.gravityOn)
						{
							currentState = State.NEEDS_NEW_WAYPOINT_GROUND;
						}
						else
						{
							currentState = State.NEEDS_NEW_WAYPOINT_AIR;
						}
						timeBeforeMove = Random.Range(_MINIMUM_TIME_BEFORE_MOVE, _MAXIMUM_TIME_BEFORE_MOVE);
						currentTimeBeforeMove = 0.0f;
					}
				}
			}
            else if (currentState == State.FIND_NEAREST_WAYPOINT)
            {
                Collider[] waypointsNearEnemy;
                if (gravitySwitch.gravityOn)
                {
                    waypointsNearEnemy = FindGroundWaypointsNearPosition(transform.position);
                }
                else
                {
                    waypointsNearEnemy = FindAirWaypointsNearPosition(transform.position);
                }

                //Look for the closest waypoint of the correct type to the enemy.
                float shortestDistance = float.MaxValue;
                Waypoint closestWaypoint = null;
                foreach (Collider waypoint in waypointsNearEnemy)
                {
                    Waypoint castedWaypoint = waypoint.gameObject.GetComponent<Waypoint>();
                    float distanceFromEnemy = CalculateEuclideanDistance(castedWaypoint.transform, transform);
                    if (distanceFromEnemy < shortestDistance)
                    {
                        shortestDistance = distanceFromEnemy;
                        closestWaypoint = castedWaypoint;
                    }
                }

                //Set is as the current waypoint.
                if (closestWaypoint != null)
                {
                    _previousWaypoint = currentWaypoint;
                    _previousWaypoint.isOccupied = false;
                    currentWaypoint = closestWaypoint;
                    closestWaypoint.isOccupied = true;
                    if (gravitySwitch.gravityOn)
                    {
                        currentState = State.MOVING_GROUND;
                        SetNewNavPosition();
                    }
                    else
                    {
                        currentState = State.MOVING_AIR;
                        StartCoroutine(FindPathToAirWaypoint());
                    }
                    arrivedAtWaypoint = false;
                }
            }
            else if (currentState == State.AT_WAYPOINT)
            {
                anim.SetFloat("Speed", 0.0f);
                anim.SetBool("Aiming", true);
                anim.SetTrigger("Shoot");

                int moveChance = Random.Range(0, 100);
                if (stayedAtWaypoint && moveChance <= chanceOfMoving)
                {
                    if (gravitySwitch.gravityOn)
                    {
                        currentState = State.NEEDS_NEW_WAYPOINT_GROUND;
                    }
                    else
                    {
                        currentState = State.NEEDS_NEW_WAYPOINT_AIR;
                    }
                    chanceOfMoving = _BASE_CHANCE_OF_MOVING;
                    stayedAtWaypoint = false;
                }
                else
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), Time.deltaTime);
                    currentState = State.SHOOTING;
                    _previousState = State.AT_WAYPOINT;
                    chanceOfMoving += _INCREASE_CHANCE_OF_MOVING;
                }
            }
        }
        else
        {
            deathTimer += Time.deltaTime;
            if (deathTimer > anim.GetCurrentAnimatorStateInfo(0).length || !gravitySwitch.gravityOn)
            {
                SetKinematicState(false);
                //ChangeRagdollJointActiveState(true);
                anim.enabled = false;
				if (gravitySwitch.gravityOn)
				{
					SetRagdollGravity(true);
				}
				else
				{
					SetRagdollGravity(false);
                }
            }
        }
	}

    //Uses a sphere overlap test to find and return the ground waypoints closest to a given position. Tries to return at least one result by increasing the search
    //range as necessary, up to a maximum.
    private Collider[] FindGroundWaypointsNearPosition(Vector3 position)
	{
		Collider[] waypointsNearPosition = Physics.OverlapSphere(position, 10, (1 << 8) | (1 << 11)); //Bit shift to get the 2 ground waypoint layers (layers 8 and 11).
		if (waypointsNearPosition.Length == 0)
		{
			//If by some chance the enemy is in a spot where a radius of 10 doesn't find a waypoint, we increase the radius by another 10.
			int radiusIncrease = 10;
			int increaseChances = 1;
			while (waypointsNearPosition.Length == 0 && increaseChances < 5)
			{
				waypointsNearPosition = Physics.OverlapSphere(position, 10 + radiusIncrease, (1 << 8) | (1 << 11)); //Bit shift to get layers 8 and 11.
				radiusIncrease += 10;
				++increaseChances;
			}
		}
		return waypointsNearPosition;
	}

    //Uses a sphere overlap test to find and return the air waypoints closest to a given position. Tries to return at least one result by increasing the search
    //range as necessary, up to a maximum.
	private Collider[] FindAirWaypointsNearPosition(Vector3 position)
	{
		Collider[] waypointsNearPosition = Physics.OverlapSphere(position, 10, (1 << 9)); //Bit shift to get the air waypoint layer (layer 9).
		if (waypointsNearPosition.Length == 0)
		{
			//If by some chance the enemy is in a spot where a radius of 10 doesn't find a waypoint, we increase the radius by another 10.
			int radiusIncrease = 10;
			int increaseChances = 1;
			while (waypointsNearPosition.Length == 0 && increaseChances < 5)
			{
				waypointsNearPosition = Physics.OverlapSphere(position, 10 + radiusIncrease, (1 << 9)); //Bit shift to get layer 9.
				radiusIncrease += 10;
				++increaseChances;
			}
		}
		return waypointsNearPosition;
	}

	private void MoveTowardsAirWaypoint(Vector3 waypointTarget)
	{
		transform.position += transform.forward * jumpSpeed * Time.deltaTime;
	}

    //Checks if the enemy is near the current desired octree node by performing an overlap sphere test from the desired octree node's location.
    //This test only searches the layer that contains body parts of the enemy. If this test returns results, we know that the enemy is close enough
    //because they are the only thing that could produce results.
	private bool EnemyNearOctreePoint(Vector3 nodeTarget)
	{
		Collider[] enemyNearOctreePoint = Physics.OverlapSphere(nodeTarget, 0.5f, (1 << 17));
		if (enemyNearOctreePoint.Length != 0)
		{
            foreach (Collider collider in enemyNearOctreePoint)
            {
                //It's possible that another enemy just happened to be near that octree node and the test picked that enemy up.
                //We need to make sure at least one of the colliders the test found belongs to this enemy.
                if (collider.gameObject.GetComponentInParent<EnemyBehaviourGrunt>()._id == _id)
                {
                    return true;
                }
            }
		}
		return false;
	}

    //Checks if the enemy is near the current desired air waypoint by performing an overlap test from this enemy's location.
    //The test only searches the layer that contains air waypoints and checks that it is indeed the air waypoint we want.
	private bool EnemyNearCurrentAirWaypoint()
	{
		Collider[] airWaypoints = Physics.OverlapSphere(transform.position, _LOCATION_ARRIVAL_RADIUS, (1 << 9));
		foreach (Collider airWaypoint in airWaypoints)
		{
			if (ReferenceEquals(airWaypoint.gameObject.GetComponent<Waypoint>(), currentWaypoint))
			{
				return true;
			}
		}
		return false;
	}

    //Reduces the enemy's health by the amount provided. Marks them as dead, switches animation states, and reports them
    //as dead to the enemy spawn script when necessary. Performs base damage functionality as well as Grunt exclusive functionality.
    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        if (health <= 0.0f && !isDead)
        {
            isDead = true;

            if (_instantiatedMuzzleFlash != null)
            {
                Destroy(_instantiatedMuzzleFlash);
            }

            if (!_ragdollSwitchOccurred)
            {
                ReplaceEnemyWithRagdoll();
            }
        }
    }

    //Performs additional state setting functionality when setting the current waypoint that can't be performed by the generic parent version.
	public override void SetCurrentWaypoint(Waypoint waypoint, bool justSpawned)
    {
        base.SetCurrentWaypoint(waypoint, justSpawned);
        if (gravitySwitch.gravityOn)
        {
            if (justSpawned)
            {
                currentState = State.SPAWNED_GROUND;
            }
            else
            {
                currentState = State.MOVING_GROUND;
            }
            SetNewNavPosition();
        }
        else
        {
            if (justSpawned)
            {
                currentState = State.SPAWNED_AIR;
            }
            else
            {
                currentState = State.MOVING_AIR;
                StartCoroutine(FindPathToAirWaypoint());
            }

        }
    }

    //Sets the gravity state of the root model's rigidbody, as well as all it's children.
    private void SetRagdollGravity(bool gravityOn)
    {
        rigidBody.useGravity = gravityOn;
        foreach (Rigidbody ragdollJoint in _ragdollJoints)
        {
            ragdollJoint.useGravity = gravityOn;
        }
    }

    //Sets whether the rigidbody and it's ragdoll points are kinematic or not.
    private void SetKinematicState(bool kinematicOn)
    {
        rigidBody.isKinematic = kinematicOn;
        foreach (Rigidbody ragdollJoint in _ragdollJoints)
        {
            ragdollJoint.isKinematic = kinematicOn;
        }
    }

    //Sets the correct position of the raycast bullet and the muzzle flash and handles the raycast bullet collision check.
    private IEnumerator WaitBetweenShots()
    {
        //Debug.Log("Begin Firing");

        //Something has a tendency to destroy the shooting position. It doesn't happen consistently or even in every playthrough, so I'm struggling to track what's doing it.
        //So just reassign it if it gets destroyed.
        if (_shootingPosition == null)
        {
            _shootingPosition = GetComponentInChildren<EnemyMuzzleFlash>().gameObject.transform;
        }

        if (_instantiatedMuzzleFlash == null)
        {
            _instantiatedMuzzleFlash = (GameObject)Instantiate(muzzleFlash, _shootingPosition.transform.position, _shootingPosition.transform.rotation);
        }
        else
        {
            _instantiatedMuzzleFlash.transform.position = _shootingPosition.transform.position;
            _instantiatedMuzzleFlash.transform.rotation = _shootingPosition.transform.rotation;
            _instantiatedMuzzleFlash.SetActive(true);
        }

        StartCoroutine(WaitBetweenFireSounds());
        for (int i = 0; i < _NUM_TIMES_SHOOT_PER_BOUT; ++i)
        {
			if (this != null)
			{
				Transform enemyShootPosition = new GameObject().transform;
                enemyShootPosition.position = transform.position + new Vector3(0, 1.75f, 0);
                enemyShootPosition.LookAt(GameObject.FindGameObjectWithTag("PlayerHead").transform);
				//transform.LookAt(target);

				RaycastHit hit = Shoot(enemyShootPosition.position, enemyShootPosition.transform.forward);
				Destroy(enemyShootPosition.gameObject);

				if (hit.collider.name == "PlayerSightCollider")
				{
                    //float distance = CalculateEuclideanDistance(transform, target);
					bool hitSuccessful = playerDamageHandler.AttemptDamage(CalculateEuclideanDistance(transform, target), (vp_Input.GetAxisRaw("Horizontal") != 0 || vp_Input.GetAxisRaw("Vertical") != 0) ? true : false, transform);

                    //if (distance >= 25.0f && distance <= 26.0f)
                    //{
                    //    ++_debugShotsFired;
                    //    if (hitSuccessful)
                    //    {
                    //        ++_debugShotsHit;
                    //    }
                    //}
				}

				//Debug.DrawRay(enemyShootPosition.position, transform.forward * 150.0f, Color.green, 5.0f);
				//Debug.Log(hit.collider.name);

				yield return new WaitForSeconds(0.1f);
			}
        }

        //Same reasoning as above.
        if (_shootingPosition == null)
        {
            _shootingPosition = GetComponentInChildren<EnemyMuzzleFlash>().gameObject.transform;
        }
        _instantiatedMuzzleFlash.SetActive(false);
        readyToFire = true;
    }

    //Controls the playing of the weapon firing sounds.
	private IEnumerator WaitBetweenFireSounds()
	{
		for (int i = 0; i < _NUM_TIMES_SHOOT_PER_BOUT; ++i)
		{
			if (this != null)
			{
				enemyFiring.Play();
				//Debug.Log("Fire");
				yield return new WaitForSeconds(1f);
			}
		}
	}

    //Replaces the enemy with a ragdoll equipped variant on death. Sets the ragdoll version to the same state as this enemy.
	public void ReplaceEnemyWithRagdoll()
	{
		GameObject ragdollEnemy = (GameObject)Instantiate(ragdollModel, transform.position, transform.rotation);;
		EnemyBehaviourGrunt behaviour = ragdollEnemy.GetComponent<EnemyBehaviourGrunt>();

		//Disable the colliders on the dead enemy so they don't block the player.
		Destroy(ragdollEnemy.transform.Find("SK_Soldier_Head").GetComponent<BoxCollider>());
		Destroy(ragdollEnemy.transform.Find("SK_Soldier_Torso").GetComponent<BoxCollider>());
		Destroy(ragdollEnemy.transform.Find("SK_Soldier_Legs").GetComponent<BoxCollider>());

		behaviour.Start();
		behaviour.currentState = EnemyBehaviourGrunt.State.DEAD;
		behaviour.isDead = true;

		behaviour.navMeshAgent.Stop();
		behaviour.navMeshAgent.enabled = false;
		ragdollEnemy.GetComponent<NavMeshObstacle>().enabled = true;

		if (gravitySwitch.gravityOn)
		{
			behaviour.SetKinematicState(true);
			behaviour.anim.SetBool("Dead", true);
		}
		else
		{
			behaviour.anim.SetFloat("Speed", 0.0f);
		}

        int deathSound = Random.Range(0, 100);
        if (deathSound < 50)
        {
            behaviour.enemyDeathGrunt1.Play();
        }
        else
        {
            behaviour.enemyDeathGrunt2.Play();
        }

        if (!batterySpawned)
        {
            int batteryChance = Random.Range(0, 100);
            if (batteryChance < 40)
            {
                GameObject battery = (GameObject)Instantiate(batteryPickup, transform.position + new Vector3(0f, 1.5f, 0f), Quaternion.identity);

                int batteryAmount = Random.Range(1, 31);
                battery.GetComponent<BatteryPickup>().amountOfBatteryPower = batteryAmount;
            }
            batterySpawned = true;
        }

        _ragdollSwitchOccurred = true;
		behaviour._ragdollSwitchOccurred = true;
		enemyDestroyer.RemoveEnemyFromArena(this);
	}

    //Briefly pauses behaviour operations when enemies transition from no gravity back to gravity.
    //This is done to give gravity a chance to bring the enemy back to the ground before behaviours kick in.
	private IEnumerator WaitForGravity()
	{
		yield return new WaitForSeconds(2f);
		navMeshAgent.enabled = true;
		_waitedForGravity = true;
		yield return new WaitForEndOfFrame();
	}

    //Calls the A* algorithm to find a path from the current air waypoint to the desired air waypoint.
	private IEnumerator FindPathToAirWaypoint()
	{
		OctreeNode startingNode = Octree.root.FindNodeContainingPoint(transform.position);
		OctreeNode endingNode = Octree.root.FindNodeContainingPoint(currentWaypoint.transform.position);
		_airPath = AStar.AStarPathfinding(startingNode, endingNode);
		yield return new WaitForEndOfFrame();
	}

    //A test version of the above function that was used to see the path produced by two specifically chosen
    //waypoints, as opposed to being based on the operation of the enemy behaviour.
    private IEnumerator FindPathToAirWaypoint_TestVersion(Waypoint testWaypoint1, Waypoint testWaypoint2)
    {
        OctreeNode startingNode = Octree.root.FindNodeContainingPoint(testWaypoint1.transform.position);
        OctreeNode endingNode = Octree.root.FindNodeContainingPoint(testWaypoint2.transform.position);
        List<OctreeNode> finalPath = AStar.AStarPathfinding(startingNode, endingNode);
        foreach (OctreeNode node in finalPath)
        {
        	node.DrawNodeGameObject(Color.green);
        }
        yield return new WaitForEndOfFrame();
    }

    //void OnDrawGizmos()
    //{
    //	Gizmos.color = Color.red;
    //	Gizmos.DrawSphere(transform.position, 1);
    //	Gizmos.color = Color.blue;
    //	Gizmos.DrawSphere(currentWaypoint.transform.position, 1);
    //}

    /*
    void DrawGroundPath()
    {
        if (navMeshAgent == null || navMeshAgent.path == null)
            return;

        var line = GetComponent<LineRenderer>();
        if (line == null)
        {
            line = gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.SetWidth(0.5f, 0.5f);
            line.SetColors(Color.yellow, Color.yellow);
        }

        var path = navMeshAgent.path;

        line.SetVertexCount(path.corners.Length);

        for (int i = 0; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }
    }
    */
}
