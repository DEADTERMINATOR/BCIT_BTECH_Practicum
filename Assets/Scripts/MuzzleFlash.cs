﻿using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour
{
    public Object MuzzleEffect;
    public Transform GunEnd;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0))
	    {
	        if (MuzzleEffect != null)
	        {
	            Destroy(MuzzleEffect);
	        }
	        GameObject pistol = GameObject.FindGameObjectWithTag("Pistol");
	        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
	        //Vector3 position = new Vector3(pistol.transform.position.x, pistol.transform.position.y,
	            //pistol.transform.position.z);
	        //position.z += pistol.transform.localPosition.z;
	        Vector3 offset = transform.forward;
            MuzzleEffect = Instantiate(MuzzleEffect, pistol.transform.position + offset, Quaternion.Euler(new Vector3(camera.transform.rotation.x, camera.transform.rotation.y, camera.transform.rotation.z)));
        }

	}
}
