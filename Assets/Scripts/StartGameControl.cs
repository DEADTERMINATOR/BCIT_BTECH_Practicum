﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartGameControl : MonoBehaviour
{
    [HideInInspector]
    public bool isGameOver = false;

    //Pistol ammo pickup automatically spawned at start so the inventory has something in reserve and reload isn't disabled.
    public GameObject pistolAmmoPickup;

    //The audio mixer. Used to set the master volume based on the options menu selection.
    public AudioMixer audioMixer;


    //Reference to the player's FPInput script to set the aiming sensitivity based on the options menu selection.
	private vp_FPInput _input;


	// Use this for initialization
	void Start ()
    {
        //Debug.Log(UnityEngine.VR.VRSettings.loadedDeviceName);
        if (GameObject.FindGameObjectWithTag("Pistol").activeSelf)
        {
            Instantiate(pistolAmmoPickup, GameObject.FindGameObjectWithTag("Player").transform.position, Quaternion.identity);
        }

        audioMixer.SetFloat("MasterVolume", -45.0f + (PlayerPrefs.GetFloat("MusicVolume") * 65.0f)); //This converts the slider range of 0.0 - 1.0 to the range of 20.0 - -45.0 used by the audio mixer.
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            //This converts the slider range of 0.1 - 1.0 to a 1.1 - 10.1 range used by the sensitivity setting.
            GameObject.FindGameObjectWithTag("Player").GetComponent<vp_FPInput>().MouseLookSensitivity = new Vector2(PlayerPrefs.GetFloat("AimingSensitivity") * 20 + 0.1f, PlayerPrefs.GetFloat("AimingSensitivity") * 20 + 0.1f);
        }
        else if (SceneManager.GetActiveScene().name == "GameSceneVR")
        {
            if (PlayerPrefs.GetInt("FullRotation") == 1)
            {
                GameObject.Find("VRModeModerate").GetComponent<vp_VRCameraManager>().SnapRotation.SnapRotate = false;
            }
            else
            {
                GameObject.Find("VRModeModerate").GetComponent<vp_VRCameraManager>().SnapRotation.SnapRotate = true;
                GameObject.Find("VRModeModerate").GetComponent<vp_VRCameraManager>().SnapRotation.StepDegrees = PlayerPrefs.GetFloat("SnapRotateDegrees") * 15; //Converts a 12-step slider into a range of 0 - 180 degrees.
            }
        }
        Cursor.lockState = CursorLockMode.Locked;
    }
}
