﻿using UnityEngine;
using System.Collections;

//Used to prevent the trigger that the enemies look for when determining whether they can see the player from rotating
//with the player's movements.
public class PlayerSightLockRotation : MonoBehaviour
{
	// Update is called once per frame
	void Update ()
    {
        transform.rotation = Quaternion.Euler(0, 0, 0);
	}
}
