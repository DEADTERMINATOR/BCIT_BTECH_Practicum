﻿using UnityEngine;
using System.Collections;

public class MenuVolumeSet : MonoBehaviour
{
	//Reference to the menu music audio source so we can change the volume on the menu based on the player's preferences.
	private AudioSource _menuMusic;


    // Use this for initialization
    void Start ()
    {
        _menuMusic = GameObject.FindGameObjectWithTag("MenuMusic").GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            _menuMusic.volume = PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            _menuMusic.volume = 0.8f;
        }
    }
}
