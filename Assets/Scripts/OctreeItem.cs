﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class OctreeItem : MonoBehaviour 
{
	//Holds the octree nodes this item belongs to.
	public List<OctreeNode> ownerNodes = new List<OctreeNode>();

	//The size of the item.
	public Vector3 size;

	//The trigger collider that representes the area the octree will use when determining which nodes contain the item.
	public BoxCollider avoidanceTrigger;


	// Use this for initialization
	void Awake() 
	{
		//size = GetComponentInChildren<Renderer>().bounds.size;
		avoidanceTrigger = GetComponent<BoxCollider>();
		size = avoidanceTrigger.bounds.size;

		Octree.root.ProcessItem(this);
	}

	void FixedUpdate() 
	{
		//Octree.root.DrawNodesWithItems();
	}
}
