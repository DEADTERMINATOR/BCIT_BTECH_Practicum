﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyDestructionManager : MonoBehaviour 
{
    //How long should the bodies of the two enemy types remain before being removed.
	public const float TIME_BEFORE_GRUNT_REMOVAL = 60.0f;
	public const float TIME_BEFORE_MECH_REMOVAL = 10.0f;


    //The list of enemies.
	public static List<EnemyBehaviour> enemies;

    //Pauses updating while the game is paused.
	public static bool pauseUpdating = false;


    //Has the last update pass completed and are we ready for another one?
	private bool readyForUpdate = true;

    //Debug variable used to see how many times the update has run in total.
    private int runCount = 0;


	// Use this for initialization
	void Start () 
	{
		enemies = new List<EnemyBehaviour>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!pauseUpdating && readyForUpdate)
		{
			readyForUpdate = false;
			//We run the update in a co-routine because the updating of this information is not critical to the current state of the game,
			//so we don't need it holding up other, more critical operations.
			StartCoroutine(ManagerUpdate());
		}
	}

	private IEnumerator ManagerUpdate()
	{
		UpdateEnemies();
		yield return new WaitForEndOfFrame();
		readyForUpdate = true;
	}

    //Loops through the enemies, checks if they are dead, and increments the death timer and calls for their removal is necessary.
	private void UpdateEnemies()
	{
		EnemyBehaviour[] enemiesCopy = new EnemyBehaviour[enemies.Count];
		enemies.CopyTo(enemiesCopy);
		for (int i = 0; i < enemiesCopy.Length; ++i)
		{
			if (enemiesCopy[i].isDead)
			{
				enemiesCopy[i].howLongDead += Time.deltaTime;
				if (enemiesCopy[i] is EnemyBehaviourGrunt)
				{
					if (enemiesCopy[i].howLongDead >= TIME_BEFORE_GRUNT_REMOVAL)
					{
						RemoveEnemyFromArena(enemiesCopy[i]);
					}
				}
				else
				{
					if (enemiesCopy[i].howLongDead >= TIME_BEFORE_MECH_REMOVAL)
					{
						RemoveEnemyFromArena(enemiesCopy[i]);
					}
				}
			}
		}
	}

	public void AddEnemyForTracking(EnemyBehaviour enemy)
	{
		enemies.Add(enemy);
	}
	
    //Removes an enemy from tracking and destroys it if it's time is up.
	public void RemoveEnemyFromArena(EnemyBehaviour enemy)
	{
		enemies.Remove(enemy);
        if (enemy != null)
        {
            GameObject.Destroy(enemy.gameObject);
        }
	}
}
